const sql = require('mssql');
const logger = require('./utils/logger');

const config = {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    server: process.env.DB_SERVER,
    database: process.env.DB_DATABASE,
    port: +process.env.DB_PORT,
    options: {
        trustServerCertificate: true,
    },
    connectionTimeout: 150000,
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000,
    }
};

// const config = {
//     server: process.env.DB_SERVER,
//     database: process.env.DB_DATABASE,
//     port: +process.env.DB_PORT,
//     driver: 'msnodesqlv8',
//     options: {
//         trustedConnection: true,
//         enableArithAbort: false
//     }
// };

const poolPromise = new sql.ConnectionPool(config)
    .connect()
    .then((pool) => {
        logger.info('Connected to MSSQL server');
        return pool;
    })
    .catch((err) =>
        logger.error('Database Connection Failed! Bad Config: ', err));

module.exports = {
    sql,
    poolPromise
};

// module.exports = config;