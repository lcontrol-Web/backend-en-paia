const socket = require('../startup/socket');

const disconnect = async (socketId) =>
    socket.disconnectSocket(socketId);

module.exports = {
    disconnect,
};