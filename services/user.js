const jwtLib = require('jsonwebtoken');
const jwt = require('../startup/jwt');
const User = require('../models/user');

const getUsers = async () =>
    User.getUsers();

const getUsersByPagination = async (pagination) =>
    User.getUsersByPagination(pagination);

const getTeamLeaders = async () =>
    User.getTeamLeaders();

const getTeamLeadersByPagination = async (pagination) =>
    User.getTeamLeadersByPagination(pagination);

const getUserByUserId = async (userId) =>
    User.getUserByUserId(userId);

const getUsersByUserGroupId = async (userGroupId) =>
    User.getUsersByUserGroupId(userGroupId);

const getAmountOfUsersByUserGroupId = async (userGroupId) =>
    User.getAmountOfUsersByUserGroupId(userGroupId);

const getUsersByUserGroupIdAndPagination = async (userGroupId, pagination) =>
    User.getUsersByUserGroupIdAndPagination(userGroupId, pagination);

const getAmountOfUsersByUserGroupIdAndPagination = async (userGroupId, pagination) =>
    User.getAmountOfUsersByUserGroupIdAndPagination(userGroupId, pagination);

const getUsersByUserGroupIdAndTeamLeaderId = async (userGroupId, teamLeaderId) =>
    User.getUsersByUserGroupIdAndTeamLeaderId(userGroupId, teamLeaderId);

const getUsersByUserGroupIdAndTeamLeaderIdAndPagination = async (userGroupId, teamLeaderId, pagination) =>
    User.getUsersByUserGroupIdAndTeamLeaderIdAndPagination(userGroupId, teamLeaderId, pagination);

const getAmountOfUsersByUserGroupIdAndTeamLeaderIdAndPagination = async (userGroupId, teamLeaderId, pagination) =>
    User.getAmountOfUsersByUserGroupIdAndTeamLeaderIdAndPagination(userGroupId, teamLeaderId, pagination);

const getUserByUserName = async (userName) =>
    User.getUserByUserName(userName);

const getUsersByTeamLeaderId = async (teamLeaderId) =>
    User.getUsersByTeamLeaderId(teamLeaderId);

const getUsersByTeamLeaderIdAndPagination = async (teamLeaderId, pagination) =>
    User.getUsersByTeamLeaderIdAndPagination(teamLeaderId, pagination);

const getUserByUserNameForAuth = async (userName) =>
    User.getUserByUserNameForAuth(userName);

const getUserByUserNameAndUserIdIsDifferent = async (userId, userName) =>
    User.getUserByUserNameAndUserIdIsDifferent(userId, userName);

const getAmountOfUsers = async () =>
    User.getAmountOfUsers();

const getAmountOfUsersByPagination = async (pagination) =>
    User.getAmountOfUsersByPagination(pagination);

const getAmountOfUsersByTeamLeaderId = async (teamLeaderId) =>
    User.getAmountOfUsersByTeamLeaderId(teamLeaderId);

const getAmountOfUsersByTeamLeaderIdAndPagination = async (teamLeaderId, pagination) =>
    User.getAmountOfUsersByTeamLeaderIdAndPagination(teamLeaderId, pagination);

const createUser = async (user) => {
    const result = await User.createUser(user);
    user.userId = result.id;
    return user;
};

const updateUser = async (userId, user) => {
    await User.updateUser(userId, user);
    return user;
};

const deleteUser = async (userId) => {
    const result = await User.delete(userId);
    return result;
};

const generateToken = (user) => {
    const privateKey = jwt.getJwt();

    const token = jwtLib.sign({
        userId: user.userId,
        userName: user.userName,
        userGroupId: user.userGroupId,
        userGroupName: user.userGroupName,
        userGroupType: user.userGroupType
    }, privateKey);

    return token;
};

module.exports = {
    getUsers,
    getUsersByPagination,
    getTeamLeaders,
    getTeamLeadersByPagination,
    getUserByUserId,
    getUserByUserName,
    getUsersByUserGroupId,
    getAmountOfUsersByUserGroupId,
    getUsersByUserGroupIdAndPagination,
    getAmountOfUsersByUserGroupIdAndPagination,
    getUsersByUserGroupIdAndTeamLeaderId,
    getUsersByUserGroupIdAndTeamLeaderIdAndPagination,
    getAmountOfUsersByUserGroupIdAndTeamLeaderIdAndPagination,
    getUsersByTeamLeaderId,
    getUsersByTeamLeaderIdAndPagination,
    getUserByUserNameForAuth,
    getUserByUserNameAndUserIdIsDifferent,
    getAmountOfUsers,
    getAmountOfUsersByPagination,
    getAmountOfUsersByTeamLeaderId,
    getAmountOfUsersByTeamLeaderIdAndPagination,
    generateToken,
    createUser,
    updateUser,
    deleteUser,
};