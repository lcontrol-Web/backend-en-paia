const Interrogation = require('../models/interrogation');

const getInterrogations = async () =>
    Interrogation.getInterrogations();

const getOpenInterrogations = async () =>
    Interrogation.getOpenInterrogations();

const getInterrogationByInterrogationId = async (interrogationId) =>
    Interrogation.getInterrogationByInterrogationId(interrogationId);

const startInterrogation = async (interrogation, userId) => {
    const result = await Interrogation.startInterrogation(interrogation, userId);
    interrogation.interrogationId = result.id;
    return interrogation;
};

const endInterrogation = async (interrogationId, interrogation) => {
    await Interrogation.endInterrogation(interrogationId, interrogation);
    return interrogation;
};

const pauseInterrogation = async (interrogationId, interrogation) => {
    await Interrogation.pauseInterrogation(interrogationId, interrogation);
    return interrogation;
};

const continueInterrogation = async (interrogationId) => {
    await Interrogation.continueInterrogation(interrogationId);
    return interrogationId;
};

const deleteInterrogation = async (interrogationId) => {
    const result = await Interrogation.deleteInterrogation(interrogationId);
    return result;
};

const deleteInterrogations = async (interrogationIds) => {
    const result = await Interrogation.deleteInterrogations(interrogationIds);
    return result;
};

module.exports = {
    getInterrogations,
    getInterrogationByInterrogationId,
    getOpenInterrogations,
    startInterrogation,
    endInterrogation,
    pauseInterrogation,
    continueInterrogation,
    deleteInterrogation,
    deleteInterrogations
};