const Nvr = require('../models/nvr');

const getNvrs = async () =>
    Nvr.getNvrs();

const getNvrsByPagination = async (pagination) =>
    Nvr.getNvrsByPagination(pagination);

const getNvrByNvrId = async (nvrId) =>
    Nvr.getNvrByNvrId(nvrId);

const getNvrByNvrName = async (userName) =>
    Nvr.getNvrByNvrNameForAuth(userName);

const getAmountOfNvrs = async () =>
    Nvr.getAmountOfNvrs();

const getAmountOfNvrsByPagination = async (pagination) =>
    Nvr.getAmountOfNvrsByPagination(pagination);

const createNvr = async (nvr) => {
    const result = await Nvr.createNvr(nvr);
    nvr.nvrId = result.id;
    return nvr;
};

const updateNvr = async (nvrId, nvr) => {
    await Nvr.updateNvr(nvrId, nvr);
    return nvr;
};

const deleteNvr = async (nvrId) => {
    const result = await Nvr.deleteNvr(nvrId);
    return result;
};

const deleteNvrs = async (nvrIds) => {
    const result = await Nvr.deleteNvrs(nvrIds);
    return result;
};

module.exports = {
    getNvrs,
    getNvrsByPagination,
    getNvrByNvrId,
    getNvrByNvrName,
    getAmountOfNvrs,
    getAmountOfNvrsByPagination,
    createNvr,
    updateNvr,
    deleteNvr,
    deleteNvrs
};