const UserGroup = require('../models/userGroup');

const getUserGroups = async () =>
    UserGroup.getUserGroups();

const getUserGroupsByPagination = async (pagination) =>
    UserGroup.getUserGroupsByPagination(pagination);

const getUserGroupByUserGroupId = async (userGroupId) =>
    UserGroup.getUserGroupByUserGroupId(userGroupId);

const getUserGroupByUserGroupName = async (userName) =>
    UserGroup.getUserGroupByUserGroupNameForAuth(userName);

const getAmountOfUserGroups = async () =>
    UserGroup.getAmountOfUserGroups();

const getAmountOfUserGroupsByPagination = async (pagination) =>
    UserGroup.getAmountOfUserGroupsByPagination(pagination);

const createUserGroup = async (userGroup) => {
    const result = await UserGroup.createUserGroup(userGroup);
    userGroup.userGroupId = result.id;
    return userGroup;
};

const updateUserGroup = async (userGroupId, userGroup) => {
    await UserGroup.updateUserGroup(userGroupId, userGroup);
    return userGroup;
};

const deleteUserGroup = async (userGroupId) => {
    const result = await UserGroup.deleteUserGroup(userGroupId);
    return result;
};

const deleteUserGroups = async (userGroupIds) => {
    const result = await UserGroup.deleteUserGroups(userGroupIds);
    return result;
};

module.exports = {
    getUserGroups,
    getUserGroupsByPagination,
    getUserGroupByUserGroupId,
    getUserGroupByUserGroupName,
    getAmountOfUserGroups,
    getAmountOfUserGroupsByPagination,
    createUserGroup,
    updateUserGroup,
    deleteUserGroup,
    deleteUserGroups
};