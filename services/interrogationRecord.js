const InterrogationRecord = require('../models/interrogationRecord');

const getInterrogationRecordsByRangeDates = async (rangeDates) =>
    InterrogationRecord.getInterrogationRecordsByRangeDates(rangeDates);

const getInterrogationRecordsByRangeDatesAndUserIdAndConnectedUsers = async (rangeDates, userId) =>
    InterrogationRecord.getInterrogationRecordsByRangeDatesAndUserIdAndConnectedUsers(rangeDates, userId);

const getInterrogationRecordsByRangeDatesAndUserId = async (rangeDates, userId) =>
    InterrogationRecord.getInterrogationRecordsByRangeDatesAndUserId(rangeDates, userId);

const getInterrogationRecordsByRangeDatesAndPagination = async (rangeDates, pagination) =>
    InterrogationRecord.getInterrogationRecordsByRangeDatesAndPagination(rangeDates, pagination);

const getAmountOfInterrogationRecordsByRangeDates = async (rangeDates) =>
    InterrogationRecord.getAmountOfInterrogationRecordsByRangeDates(rangeDates);

const getAmountOfInterrogationRecordsByRangeDatesAndPagination = async (rangeDates, pagination) =>
    InterrogationRecord.getAmountOfInterrogationRecordsByRangeDatesAndPagination(rangeDates, pagination);

const getTextEditorAndChatMessagesByInterrogationId = async (interrogationId) =>
    InterrogationRecord.getTextEditorAndChatMessagesByInterrogationId(interrogationId);

const getPauseAndContinueTimesByInterrogationId = async (interrogationId) =>
    InterrogationRecord.getPauseAndContinueTimesByInterrogationId(interrogationId);

const sendToArchive = async (interrogationId) =>
    InterrogationRecord.sendToArchive(interrogationId);

const sendToBurn = async (interrogationId) =>
    InterrogationRecord.sendToBurn(interrogationId);

module.exports = {
    getInterrogationRecordsByRangeDates,
    getInterrogationRecordsByRangeDatesAndUserIdAndConnectedUsers,
    getInterrogationRecordsByRangeDatesAndUserId,
    getInterrogationRecordsByRangeDatesAndPagination,
    getAmountOfInterrogationRecordsByRangeDates,
    getAmountOfInterrogationRecordsByRangeDatesAndPagination,
    getTextEditorAndChatMessagesByInterrogationId,
    getPauseAndContinueTimesByInterrogationId,
    sendToArchive,
    sendToBurn,
};