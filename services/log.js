const Log = require('../models/log');

const getLogsByRangeDates = async (rangeDates) =>
    Log.getLogsByRangeDates(rangeDates);

const getLogsByRangeDatesAndPagination = async (rangeDates, pagination) =>
    Log.getLogsByRangeDatesAndPagination(rangeDates, pagination);

const getTeamLeaderLogsByRangeDates = async (teamLeaderId, rangeDates) =>
    Log.getTeamLeaderLogsByRangeDates(teamLeaderId, rangeDates);

const getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDates = async (teamLeaderId, rangeDates) =>
    Log.getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDates(teamLeaderId, rangeDates);

const getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDatesPagination = async (teamLeaderId, rangeDates, pagination) =>
    Log.getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDatesPagination(teamLeaderId, rangeDates, pagination);

const getAmountOfLogsByRangeDates = async (rangeDates) =>
    Log.getAmountOfLogsByRangeDates(rangeDates);

const getAmountOfLogsByRangeDatesAndPagination = async (rangeDates, pagination) =>
    Log.getAmountOfLogsByRangeDatesAndPagination(rangeDates, pagination);

const createLog = async (userId, action) =>
    Log.createLog(userId, action);

module.exports = {
    getLogsByRangeDates,
    getLogsByRangeDatesAndPagination,
    getAmountOfLogsByRangeDates,
    getAmountOfLogsByRangeDatesAndPagination,
    getTeamLeaderLogsByRangeDates,
    getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDates,
    getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDatesPagination,
    createLog,
};