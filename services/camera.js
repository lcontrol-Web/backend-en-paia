const Cameras = require('../models/camera');

const getCameras = async () =>
    Cameras.getCameras();

const getCamerasByPagination = async (pagination) =>
    Cameras.getCamerasByPagination(pagination);

const getCamerasByNvrId = async (nvrId) =>
    Cameras.getCamerasByNvrId(nvrId);

const getCamerasByNvrIdAndPagination = async (nvrId, pagination) =>
    Cameras.getCamerasByNvrIdAndPagination(nvrId, pagination);

const getCameraByCameraId = async (cameraId) =>
    Cameras.getCameraByCameraId(cameraId);

const getAmountOfCameras = async () =>
    Cameras.getAmountOfCameras();

const getAmountOfCamerasByNvrId = async (nvrId) =>
    Cameras.getAmountOfCamerasByNvrId(nvrId);

const getAmountOfCamerasByPagination = async (pagination) =>
    Cameras.getAmountOfCamerasByPagination(pagination);

const getAmountOfCamerasByNvrIdAndPagination = async (nvrId, pagination) =>
    Cameras.getAmountOfCamerasByNvrIdAndPagination(nvrId, pagination);

const createCamera = async (camera) => {
    const result = await Cameras.createCamera(camera);
    camera.cameraId = result.id;
    return camera;
};

const updateCamera = async (cameraId, camera) => {
    await Cameras.updateCamera(cameraId, camera);
    return camera;
};

const deleteCamera = async (cameraId) => {
    const result = await Cameras.deleteCameras(cameraId);
    return result;
};

const deleteCameras = async (cameraIds) => {
    const result = await Cameras.deleteCameras(cameraIds);
    return result;
};

module.exports = {
    getCameras,
    getCamerasByPagination,
    getCamerasByNvrId,
    getCamerasByNvrIdAndPagination,
    getCameraByCameraId,
    getAmountOfCameras,
    getAmountOfCamerasByNvrId,
    getAmountOfCamerasByPagination,
    getAmountOfCamerasByNvrIdAndPagination,
    createCamera,
    updateCamera,
    deleteCamera,
    deleteCameras
};