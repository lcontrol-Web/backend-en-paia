class Pagination {
    constructor(pagination) {
        this.limit = pagination.limit;
        this.page = pagination.page;
        this.offset = pagination.offset;
        this.sort = pagination.sort;
        this.column = pagination.column;
        this.searchText = pagination.searchText;
        this.condition = pagination.condition;
    }
}

module.exports = Pagination;