class InterrogationPause {
    constructor(interrogationPause) {
        this.interrogationId = interrogationPause.interrogationId;
        this.editorText = interrogationPause.editorText;
        this.chatMessages = interrogationPause.chatMessages;
    }
}