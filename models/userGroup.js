const {
    sql,
    poolPromise
} = require('../db');
const queryHelper = require('../helpers/queryHelper');

const defaultColumn = 'userGroupId';
const searchColumns = ['userGroupName', 'userGroupType'];

class UserGroup {
    constructor(userGroup) {
        this.userGroupId = userGroup.userGroupId;
        this.userGroupName = userGroup.userGroupName;
        this.userGroupType = userGroup.userGroupType;
    }
}

UserGroup.getUserGroups = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT * 
                FROM userGroups
                ORDER BY userGroupId DESC;`);
    return result.recordset;
};

UserGroup.getUserGroupsByPagination = async (pagination) => {
    const pool = await poolPromise;
    let query = `SELECT * 
                 FROM userGroups `;

    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .query(query);
    return result.recordset;
};

UserGroup.getUserGroupByUserGroupId = async (userGroupId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userGroupId', sql.Int, userGroupId)
        .query(`SELECT * 
                FROM userGroups
                WHERE userGroupId = @userGroupId;`);
    return result.recordset[0];
};

UserGroup.getUserGroupByUserGroupName = async (userGroupName) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userGroupName', sql.NVarChar, userGroupName)
        .query(`SELECT * 
                FROM userGroups
                WHERE userGroupName = @userGroupName;`);
    return result.recordset[0];
};

UserGroup.getAmountOfUserGroups = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT COUNT(*) AS amount 
                FROM userGroups;`);
    return result.recordset[0].amount;
};

UserGroup.getAmountOfUserGroupsByPagination = async (pagination) => {
    const pool = await poolPromise;
    let query = `SELECT COUNT(*) AS amount 
                 FROM userGroups `;
    query += queryHelper.getSearchQuery(pagination, searchColumns);
    const result = await pool.request()
        .query(query);
    return result.recordset[0].amount;
};

UserGroup.createUserGroup = async (userGroup) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userGroupName', sql.NVarChar, userGroup.userGroupName)
        .input('userGroupType', sql.NVarChar, userGroup.userGroupType)
        .query(`INSERT INTO userGroups (userGroupName, userGroupType)
                VALUES (@userGroupName, @userGroupType); 
                SELECT SCOPE_IDENTITY() AS id;`);
    return result.recordset[0];
};

UserGroup.updateUserGroup = async (userGroupId, userGroup) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userGroupName', sql.NVarChar, userGroup.userGroupName)
        .input('userGroupType', sql.NVarChar, userGroup.userGroupType)
        .input('userGroupId', sql.Int, userGroupId)
        .query(`UPDATE userGroups 
                SET
                    userGroupName = @userGroupName,
                    userGroupType = @userGroupType
                WHERE userGroupId = @userGroupId;`);
    return result;
};

UserGroup.deleteUserGroup = async (userGroupId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userGroupId', sql.Int, userGroupId)
        .query(`DELETE FROM userGroups 
                WHERE userGroupId = @userGroupId`);
    return result;
};

UserGroup.deleteUserGroups = async (userGroupIds) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userGroupId', sql.Int, userGroupIds)
        .query(`DELETE FROM userGroups 
                WHERE userGroupId = @userGroupId`);
    return result;
};

module.exports = UserGroup;