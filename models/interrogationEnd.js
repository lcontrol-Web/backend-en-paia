class InterrogationEnd {
    constructor(interrogationEnd) {
        this.interrogationId = interrogationEnd.interrogationId;
        this.editorText = interrogationEnd.editorText;
        this.chatMessages = interrogationEnd.chatMessages;
        this.endTime = interrogationEnd.endTime;
    }
}