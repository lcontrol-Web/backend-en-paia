const {
    sql,
    poolPromise
} = require('../db');

class Interrogation {
    constructor(interrogation) {
        this.interrogationId = interrogation.interrogationId;
        this.nvrId = interrogation.nvrId;
        this.cameraId = interrogation.cameraId;
        this.investigatorNvrId = interrogation.investigatorNvrId;
        this.investigatorCameraId = interrogation.investigatorCameraId;
        this.foreignName = interrogation.foreignName;
        this.caseNumber = interrogation.caseNumber;
        this.interviewNumberEitan = interrogation.interviewNumberEitan;
        this.foreignIdNumber = interrogation.foreignIdNumber;
        this.nationality = interrogation.nationality;
        this.userId = interrogation.userId;
        this.editorText = interrogation.editorText;
        this.chatMessages = interrogation.chatMessages;
        this.startTime = interrogation.startTime;
        this.endTime = interrogation.endTime;
        this.burned = interrogation.burned;
        this.uploaded = interrogation.uploaded;
    }
}

Interrogation.getInterrogations = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT interrogations.interrogationId, interrogations.cameraId, interrogations.nvrId,
                       interrogations.investigatorCameraId, interrogations.investigatorNvrId, interrogations.foreignName, 
                       interrogations.caseNumber, interrogations.interviewNumberEitan, interrogations.foreignIdNumber, 
                       interrogations.nationality, interrogations.startTime, interrogations.endTime, 
                       interrogations.burned, interrogations.uploaded, cameras.cameraName, nvrs.nvrName, users.userName,
                       t1.cameraName AS 'investigatorCameraName', t2.nvrName AS 'investigatorNvrName'
                FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2
                WHERE cameras.cameraId = interrogations.cameraId
                AND nvrs.nvrId = interrogations.nvrId
                AND users.userId = interrogations.userId
                AND t1.cameraId = interrogations.investigatorCameraId
                AND t2.nvrId = interrogations.investigatorNvrId;`);
    return result.recordset;
};

Interrogation.getOpenInterrogations = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT interrogations.*, cameras.cameraName, nvrs.nvrName, users.userName,
                       t1.cameraName AS 'investigatorCameraName', t2.nvrName AS 'investigatorNvrName'
                FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2
                WHERE cameras.cameraId = interrogations.cameraId
                AND nvrs.nvrId = interrogations.nvrId
                AND users.userId = interrogations.userId
                AND t1.cameraId = interrogations.investigatorCameraId
                AND t2.nvrId = interrogations.investigatorNvrId
                AND dbo.interrogations.endTime IS NULL;`);
    return result.recordset;
};

Interrogation.getInterrogationByInterrogationId = async (interrogationId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('interrogationId', sql.Int, interrogationId)
        .query(`SELECT * 
                FROM interrogations
                WHERE interrogationId = @interrogationId;`);
    return result.recordset[0];
};

Interrogation.startInterrogation = async (interrogation, userId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrId', sql.Int, interrogation.nvrId)
        .input('cameraId', sql.Int, interrogation.cameraId)
        .input('investigatorNvrId', sql.Int, interrogation.investigatorNvrId)
        .input('investigatorCameraId', sql.Int, interrogation.investigatorCameraId)
        .input('foreignName', sql.NVarChar, interrogation.foreignName)
        .input('caseNumber', sql.Int, interrogation.caseNumber)
        .input('interviewNumberEitan', sql.Int, interrogation.interviewNumberEitan)
        .input('foreignIdNumber', sql.NVarChar, interrogation.foreignIdNumber)
        .input('nationality', sql.NVarChar, interrogation.nationality)
        .input('userId', sql.Int, userId)
        .input('startTime', sql.DateTime, new Date())
        .query(`INSERT INTO interrogations (nvrId, cameraId, investigatorNvrId, investigatorCameraId, foreignName, 
                            caseNumber, interviewNumberEitan, foreignIdNumber, nationality, userId, startTime)
                VALUES (@nvrId, @cameraId, @investigatorNvrId, @investigatorCameraId,
                        @foreignName, @caseNumber, @interviewNumberEitan, @foreignIdNumber, 
                        @nationality, @userId, @startTime); 
                SELECT SCOPE_IDENTITY() AS id;`);
    return result.recordset[0];
};

Interrogation.endInterrogation = async (interrogationId, interrogationEnd) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('editorText', sql.NVarChar, interrogationEnd.editorText)
        .input('chatMessages', sql.NVarChar, interrogationEnd.chatMessages)
        .input('endTime', sql.DateTime, new Date())
        .input('interrogationId', sql.Int, interrogationId)
        .query(`UPDATE interrogations 
                SET 
                    editorText = @editorText, 
                    chatMessages = @chatMessages, 
                    endTime = @endTime
                WHERE interrogationId = @interrogationId;`);
    return result.recordset;
};

Interrogation.pauseInterrogation = async (interrogationId, interrogationPause) => {
    const pool = await poolPromise;
    const result1 = await pool.request()
        .input('editorText', sql.NVarChar, interrogationPause.editorText)
        .input('chatMessages', sql.NVarChar, interrogationPause.chatMessages)
        .input('interrogationId', sql.Int, interrogationId)
        .query(`UPDATE interrogations 
                SET editorText = @editorText,
                    chatMessages = @chatMessages
                WHERE interrogationId = @interrogationId;`);

    const result2 = await pool.request()
        .input('interrogationId', sql.Int, interrogationId)
        .input('pauseTime', sql.DateTime, new Date())
        .query(`INSERT INTO pauseInterrogations (interrogationId, pauseTime)
                VALUES (@interrogationId, @pauseTime);
                SELECT SCOPE_IDENTITY() AS id;`);

    return {
        result1,
        result2
    };
};

Interrogation.continueInterrogation = async (interrogationId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('continueTime', sql.DateTime, new Date())
        .input('interrogationId', sql.Int, interrogationId)
        .query(`UPDATE pauseInterrogations 
                SET 
                    continueTime = @continueTime 
                WHERE interrogationId = @interrogationId
                AND continueTime IS NULL;`);
    return result;
};

Interrogation.deleteInterrogation = async (interrogationId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('interrogationId', sql.Int, interrogationId)
        .query(`DELETE FROM interrogations 
                WHERE interrogationId = @interrogationId;`);
    return result;
};

module.exports = Interrogation;