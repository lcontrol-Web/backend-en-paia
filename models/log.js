const {
    sql,
    poolPromise
} = require('../db');
const queryHelper = require('../helpers/queryHelper');

const defaultColumn = 'logId';
const searchColumns = ['action'];

class Log {
    constructor(log) {
        this.logId = log.nvrId;
        this.userId = log.userId;
        this.action = log.action;
        this.time = log.time;
    }
}

Log.getLogs = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT logs.logId, users.userId, users.userName, logs.action, logs.time 
                FROM logs 
                JOIN users
                ON users.userId = logs.userId;`);
    return result.recordset;
};

Log.getLogsByRangeDates = async (rangeDates) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .query(`SELECT logs.logId, users.userId, users.userName, logs.action, logs.time 
                FROM logs 
                JOIN users
                ON users.userId = logs.userId
                WHERE logs.time >= @start and logs.time <= @end 
                ORDER BY logs.time DESC;`);
    return result.recordset;
};

Log.getLogsByRangeDatesAndPagination = async (rangeDates, pagination) => {
    const pool = await poolPromise;
    let query = `SELECT logs.logId, users.userId, users.userName, logs.action, logs.time 
                FROM logs 
                JOIN users
                ON users.userId = logs.userId
                WHERE logs.time >= @start and logs.time <= @end
                ORDER BY logs.time DESC;`;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .query(query);
    return result.recordset;
};

Log.getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDates = async (teamLeaderId, rangeDates) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT logs.logId, users.userId, users.userName, logs.action, logs.time 
                FROM logs 
                JOIN users
                ON users.userId = logs.userId;`);
    return result.recordset;
};

Log.getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDatesPagination = async (teamLeaderId, rangeDates, pagination) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT logs.logId, users.userId, users.userName, logs.action, logs.time 
                FROM logs 
                JOIN users
                ON users.userId = logs.userId;`);
    return result.recordset;
};

Log.getAmountOfLogsByRangeDates = async (rangeDates) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT logs.logId, users.userId, users.userName, logs.action, logs.time 
                FROM logs 
                JOIN users
                ON users.userId = logs.userId;`);
    return result.recordset;
};

Log.getAmountOfLogsByRangeDatesAndPagination = async (rangeDates, pagination) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT logs.logId, users.userId, users.userName, logs.action, logs.time 
                FROM logs 
                JOIN users
                ON users.userId = logs.userId;`);
    return result.recordset;
};

Log.getTeamLeaderLogsByRangeDates = async (teamLeaderId, rangeDates) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .input('userId', sql.Int, teamLeaderId)
        .query(`SELECT logs.logId, users.userId, users.userName, logs.action, logs.time 
                FROM logs 
                JOIN users
                ON users.userId = logs.userId
                WHERE logs.time >= @start and logs.time <= @end
                AND logs.userId = @userId 
                UNION ALL
                SELECT logs.logId, t1.userId, t1.userName, logs.action, logs.time 
                FROM logs , (SELECT users.userId, users.userName 
                             FROM users
                             WHERE users.teamLeaderId = @userId) AS t1
                WHERE logs.time >= @start and logs.time <= @end 
                AND logs.userId = t1.userId 
                ORDER BY logs.time DESC;`);
    return result.recordset;
};

Log.createLog = async (userId, action) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userId', sql.Int, userId)
        .input('action', sql.NVarChar, action)
        .input('time', sql.DateTime, new Date())
        .query(`INSERT INTO logs (userId, action, time)
                VALUES (@userId, @action, @time); 
                SELECT SCOPE_IDENTITY() AS id;`);
    return result.recordset[0];
};

module.exports = Log;