const {
    sql,
    poolPromise
} = require('../db');

const queryHelper = require('../helpers/queryHelper');

const defaultColumn = 'nvrId';
const searchColumns = ['nvrName', 'ip', 'port', 'sdkPort', 'rtspPort', 'channels'];

class Nvr {
    constructor(nvr) {
        this.nvrId = nvr.nvrId;
        this.nvrName = nvr.nvrName;
        this.ip = nvr.ip;
        this.channels = nvr.channels;
        this.userName = nvr.userName;
        this.password = nvr.password;
        this.port = nvr.port;
        this.sdkPort = nvr.sdkPort;
        this.rtspPort = nvr.rtspPort;
    }
}

Nvr.getNvrs = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT * 
                FROM nvrs 
                ORDER BY nvrId DESC;`);
    return result.recordset;
};

Nvr.getNvrsByPagination = async (pagination) => {
    const pool = await poolPromise;
    let query = `SELECT * 
                 FROM nvrs `;

    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .query(query);
    return result.recordset;
};

Nvr.getNvrByNvrId = async (nvrId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrId', sql.Int, nvrId)
        .query(`SELECT * 
                FROM nvrs
                WHERE nvrId = @nvrId;`);
    return result.recordset[0];
};

Nvr.getAmountOfNvrs = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT COUNT(*) AS amount 
                FROM nvrs;`);
    return result.recordset[0].amount;
};

Nvr.getAmountOfNvrsByPagination = async (pagination) => {
    const pool = await poolPromise;
    let query = `SELECT COUNT(*) AS amount 
                 FROM nvrs `;
    query += queryHelper.getSearchQuery(pagination, searchColumns);
    const result = await pool.request()
        .query(query);
    return result.recordset[0].amount;
};

Nvr.createNvr = async (nvr) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrName', sql.NVarChar, nvr.nvrName)
        .input('ip', sql.NVarChar, nvr.ip)
        .input('channels', sql.Int, nvr.channels)
        .input('userName', sql.NVarChar, nvr.userName)
        .input('password', sql.NVarChar, nvr.password)
        .query(`INSERT INTO nvrs (nvrName, ip, channels, userName, password, port, sdkPort, rtspPort)
                VALUES (@nvrName, @ip, @channels, @userName, @password, 80, 8001, 554); 
                SELECT SCOPE_IDENTITY() AS id;`);
    return result.recordset[0];
};

Nvr.updateNvr = async (nvrId, nvr) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrName', sql.NVarChar, nvr.nvrName)
        .input('ip', sql.NVarChar, nvr.ip)
        .input('channels', sql.Int, nvr.channels)
        .input('userName', sql.NVarChar, nvr.userName)
        .input('password', sql.NVarChar, nvr.password)
        .input('nvrId', sql.Int, nvrId)
        .query(`UPDATE nvrs 
                SET 
                    nvrName = @nvrName, 
                    ip = @ip,
                    channels = @channels,
                    userName = @userName,
                    password = @password
                WHERE nvrId = @nvrId;`);
    return result;
};

Nvr.deleteNvr = async (nvrId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrId', sql.Int, nvrId)
        .query(`DELETE FROM nvrs 
                WHERE nvrId = @nvrId`);
    return result;
};

module.exports = Nvr;