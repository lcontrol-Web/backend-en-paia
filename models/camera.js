const {
    sql,
    poolPromise
} = require('../db');

const queryHelper = require('../helpers/queryHelper');

const defaultColumn = 'cameraId';
const searchColumns = ['cameraName', 'channel', 'cameraIp', 'controllerIp'];

class Camera {
    constructor(camera) {
        this.cameraId = camera.cameraId;
        this.nvrId = camera.nvrId;
        this.cameraName = camera.cameraName;
        this.channel = camera.channel;
        this.cameraIp = camera.cameraIp;
        this.controllerIp = camera.controllerIp;
        this.indexArray = camera.indexArray;
    }
}

Camera.getCameras = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT * 
                FROM cameras
                ORDER BY cameraId DESC;`);
    return result.recordset;
};

Camera.getCamerasByPagination = async (pagination) => {
    const pool = await poolPromise;
    let query = `SELECT * 
                 FROM cameras `;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .query(query);
    return result.recordset;
};

Camera.getCamerasByNvrId = async (nvrId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrId', sql.Int, nvrId)
        .query(`SELECT * 
                FROM cameras 
                WHERE nvrId = @nvrId
                ORDER BY cameraId DESC;`);
    return result.recordset;
};

Camera.getCamerasByNvrIdAndPagination = async (nvrId, pagination) => {
    const pool = await poolPromise;
    let query = `SELECT *
                 FROM (SELECT * 
                       FROM cameras 
                       WHERE nvrId = @nvrId) AS t1 `;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .input('nvrId', sql.Int, nvrId)
        .query(query);
    return result.recordset;
};

Camera.getCameraByCameraId = async (cameraId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('cameraId', sql.Int, cameraId)
        .query(`SELECT * 
                FROM cameras
                WHERE cameraId = @cameraId;`);
    return result.recordset[0];
};

Camera.getAmountOfCameras = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT COUNT(*) AS amount
                FROM cameras;`);
    return result.recordset[0].amount;
};

Camera.getAmountOfCamerasByNvrId = async (nvrId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrId', sql.Int, nvrId)
        .query(`SELECT COUNT(*) AS amount
                FROM cameras
                WHERE nvrId = @nvrId;`);
    return result.recordset[0].amount;
};

Camera.getAmountOfCamerasByNvrIdAndPagination = async (nvrId, pagination) => {
    const pool = await poolPromise;
    let query = `SELECT *
                 FROM (SELECT *
                       FROM cameras
                       WHERE nvrId = @nvrId) AS t1 `;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .input('nvrId', sql.Int, nvrId)
        .query(query);
    const amount = result.recordset.length;
    return amount;
};

Camera.createCamera = async (camera) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrId', sql.Int, camera.nvrId)
        .input('cameraName', sql.NVarChar, camera.cameraName)
        .input('channel', sql.Int, camera.channel)
        .input('cameraIp', sql.NVarChar, camera.cameraIp)
        .input('controllerIp', sql.NVarChar, camera.controllerIp)
        .input('indexArray', sql.Int, camera.indexArray)
        .query(`INSERT INTO cameras (nvrId, cameraName, channel, cameraIp, controllerIp, indexArray)
                VALUES (@nvrId, @cameraName, @channel, @cameraIp, @controllerIp, @indexArray); 
                SELECT SCOPE_IDENTITY() AS id;`);
    return result.recordset[0];
};

Camera.updateCamera = async (cameraId, camera) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('nvrId', sql.Int, camera.nvrId)
        .input('cameraName', sql.NVarChar, camera.cameraName)
        .input('channel', sql.Int, camera.channel)
        .input('cameraIp', sql.NVarChar, camera.cameraIp)
        .input('controllerIp', sql.NVarChar, camera.controllerIp)
        .input('indexArray', sql.Int, camera.indexArray)
        .input('cameraId', sql.Int, cameraId)
        .query(`UPDATE cameras 
                SET 
                    nvrId = @nvrId, 
                    cameraName = @cameraName,
                    channel = @channel,
                    cameraIp = @cameraIp,
                    controllerIp = @controllerIp,
                    indexArray = @indexArray
                WHERE cameraId =  @cameraId;`);
    return result;
};

Camera.deleteCamera = async (cameraId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('cameraId', sql.Int, cameraId)
        .query(`DELETE FROM cameras 
                WHERE cameraId = @cameraId`);
    return result;
};

module.exports = Camera;