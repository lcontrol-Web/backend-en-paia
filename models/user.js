const Joi = require('joi');
const {
    sql,
    poolPromise
} = require('../db');

const queryHelper = require('../helpers/queryHelper');

const defaultColumn = 'userId';
const searchColumns = ['userName'];

class User {
    constructor(user) {
        this.userId = user.userId;
        this.userName = user.userName;
        this.userGroupId = user.userGroupId;
        this.userGroupName = user.userGroupName;
        this.userGroupType = user.userGroupType;
        this.teamLeaderId = user.teamLeaderId;
    }
}

User.getUsers = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT t1.userId, t1.userName, t1.userGroupId, t1.teamLeaderId, users.userName AS teamLeaderName
                FROM users, users AS t1
                WHERE users.userId = t1.teamLeaderId
                UNION
                SELECT users.*, NULL AS teamLeaderName
                FROM users 
                WHERE users.teamLeaderId IS NULL;`);
    return result.recordset;
};

User.getUsersByPagination = async (pagination) => {
    const pool = await poolPromise;
    let query = `SELECT t1.userId, t1.userName, t1.userGroupId, t1.teamLeaderId, users.userName AS teamLeaderName
                 FROM users, users AS t1
                 WHERE users.userId = t1.teamLeaderId
                 UNION
                 SELECT users.*, NULL AS teamLeaderName
                 FROM users 
                 WHERE users.teamLeaderId IS NULL;`;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .query(query);
    return result.recordset;
};

User.getUserByUserId = async (userId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userId', sql.Int, userId)
        .query(`SELECT * 
                FROM users
                WHERE userId = @userId`);
    return result.recordset[0];
};

User.getUserByUserName = async (userName) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userName', sql.NVarChar, userName)
        .query(`SELECT * 
                FROM users
                WHERE userName = @userName`);
    return result.recordset[0];
};

User.getUsersByUserGroupId = async (userGroupId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userGroupId', sql.Int, userGroupId)
        .query(`SELECT * 
                FROM users
                WHERE userGroupId = @userGroupId`);
    return result.recordset[0];
};

User.getAmountOfUsersByUserGroupId = async (userGroupId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userGroupId', sql.Int, userGroupId)
        .query(`SELECT COUNT(*) AS amount 
                FROM users
                WHERE userGroupId = @userGroupId;`);
    return result.recordset[0].amount;
};

User.getUsersByUserGroupIdAndPagination = async (userGroupId, pagination) => {
    const pool = await poolPromise;
    let query = `SELECT * 
                 FROM (SELECT * 
                      FROM (SELECT t1.userId, t1.userName, t1.userGroupId, t1.teamLeaderId, users.userName AS teamLeaderName
                            FROM users, users AS t1
                            WHERE users.userId = t1.teamLeaderId
                            UNION
                            SELECT users.*, NULL AS teamLeaderName
                            FROM users 
                            WHERE users.teamLeaderId IS NULL) AS t3
                      WHERE t3.userGroupId = @userGroupId) AS t4 `;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .input('userGroupId', sql.Int, userGroupId)
        .query(query);
    return result.recordset;
};

User.getAmountOfUsersByUserGroupIdAndPagination = async (userGroupId, pagination) => {
    const pool = await poolPromise;
    let query = `SELECT *
                 FROM (SELECT *
                       FROM users 
                       WHERE userGroupId = @userGroupId) AS t1`;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .input('userGroupId', sql.Int, userGroupId)
        .query(query);
    const amount = result.recordset.length;
    return amount;
};

User.getUsersByTeamLeaderId = async (teamLeaderId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('teamLeaderId', sql.Int, teamLeaderId)
        .query(`SELECT * 
                FROM users
                WHERE teamLeaderId = @teamLeaderId`);
    return result.recordset;
};

User.getUserByUserNameForAuth = async (userName) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userName', sql.NVarChar, userName)
        .query(`SELECT users.*, userGroups.userGroupName, userGroups.userGroupType
                FROM users, userGroups
                WHERE userName = @userName 
                AND users.userGroupId = userGroups.userGroupId`);
    return result.recordset[0];
};

User.getUserByUserNameAndUserIdIsDifferent = async (userId, userName) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userName', sql.NVarChar, userName)
        .input('userId', sql.Int, userId)
        .query(`SELECT * 
                FROM users 
                WHERE userName = @userName
                AND userId <> @userId;`);
    return result.recordset[0];
};

User.getTeamLeaders = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT users.* 
                FROM userGroups, users 
                WHERE userGroups.userGroupType = 'team leader'
                AND userGroups.userGroupId = users.userGroupId`);
    return result.recordset;
};

User.getAmountOfUsers = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT COUNT(*) AS amount 
                FROM users;`);
    return result.recordset[0].amount;
};

User.getAmountOfUsersByPagination = async (pagination) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT users.* 
                FROM userGroups, users 
                WHERE userGroups.userGroupType = 'team leader'
                AND userGroups.userGroupId = users.userGroupId `);
    return result.recordset;
};

User.createUser = async (user) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userName', sql.NVarChar, user.userName)
        .input('userGroupId', sql.Int, user.userGroupId)
        .input('teamLeaderId', sql.Int, user.teamLeaderId)
        .query(`INSERT INTO users (userName, userGroupId, teamLeaderId) 
                VALUES (@userName, @userGroupId, @teamLeaderId);
                SELECT SCOPE_IDENTITY() AS id;`);
    return result.recordset[0];
};

User.updateUser = async (userId, user) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userName', sql.NVarChar, user.userName)
        .input('userGroupId', sql.Int, user.userGroupId)
        .input('teamLeaderId', sql.Int, user.teamLeaderId)
        .input('userId', sql.Int, userId)
        .query(`UPDATE users 
                SET
                    userName = @userName,
                    userGroupId = @userGroupId,
                    teamLeaderId = @teamLeaderId
                WHERE userId = @userId;`);
    return result;
};

User.deleteUser = async (userId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('userId', sql.Int, userId)
        .query(`DELETE FROM users 
                WHERE userId = @userId`);
    return result;
};

module.exports = User;