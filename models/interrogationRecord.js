const {
    sql,
    poolPromise
} = require('../db');
const queryHelper = require('../helpers/queryHelper');

const defaultColumn = 'interrogationId';
const searchColumns = [
    'foreignName',
    'caseNumber',
    'interviewNumberEitan',
    'foreignIdNumber',
    'nationality',
    'cameraName',
    'nvrName',
    'investigatorNvrName',
    'investigatorCameraName',
    'userName'
];

class InterrogationRecord {
    constructor(interrogationRecord) {
        this.interrogationId = interrogationRecord.interrogationId;
        this.nvrId = interrogationRecord.nvrId;
        this.cameraId = interrogationRecord.cameraId;
        this.investigatorNvrId = interrogationRecord.investigatorNvrId;
        this.investigatorCameraId = interrogationRecord.investigatorCameraId;
        this.foreignName = interrogationRecord.foreignName;
        this.caseNumber = interrogationRecord.caseNumber;
        this.interviewNumberEitan = interrogationRecord.interviewNumberEitan;
        this.foreignIdNumber = interrogationRecord.foreignIdNumber;
        this.nationality = interrogationRecord.nationality;
        this.userId = interrogationRecord.userId;
        this.editorText = interrogationRecord.editorText;
        this.chatMessages = interrogationRecord.chatMessages;
        this.startTime = interrogationRecord.startTime;
        this.endTime = interrogationRecord.endTime;
        this.burned = interrogationRecord.burned;
        this.uploaded = interrogationRecord.uploaded;
    }
}

InterrogationRecord.getInterrogationRecordsByRangeDates = async (rangeDates) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .query(`SELECT interrogations.interrogationId, interrogations.cameraId, interrogations.nvrId,
                       interrogations.investigatorCameraId, interrogations.investigatorNvrId, interrogations.foreignName, 
                       interrogations.caseNumber, interrogations.interviewNumberEitan, interrogations.foreignIdNumber, 
                       interrogations.nationality, interrogations.startTime, interrogations.endTime, 
                       interrogations.burned, interrogations.uploaded, cameras.cameraName, nvrs.nvrName, users.userName,
                       t1.cameraName AS 'investigatorCameraName', t2.nvrName AS 'investigatorNvrName'
                FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2
                WHERE cameras.cameraId = interrogations.cameraId
                AND nvrs.nvrId = interrogations.nvrId
                AND users.userId = interrogations.userId
                AND t1.cameraId = interrogations.investigatorCameraId
                AND t2.nvrId = interrogations.investigatorNvrId
                AND interrogations.startTime >= @start 
                AND interrogations.endTime <= @end 
                ORDER BY interrogations.endTime DESC;`);
    return result.recordset;
};

InterrogationRecord.getInterrogationRecordsByRangeDatesAndPagination = async (rangeDates, pagination) => {
    const pool = await poolPromise;
    let query = `SELECT *
                 FROM (SELECT interrogations.interrogationId, interrogations.cameraId, interrogations.nvrId,
                        interrogations.investigatorCameraId, interrogations.investigatorNvrId, interrogations.foreignName, 
                        interrogations.caseNumber, interrogations.interviewNumberEitan, interrogations.foreignIdNumber, 
                        interrogations.nationality, interrogations.startTime, interrogations.endTime, 
                        interrogations.burned, interrogations.uploaded, cameras.cameraName, nvrs.nvrName, users.userName,
                        t1.cameraName AS 'investigatorCameraName', t2.nvrName AS 'investigatorNvrName'
                      FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2
                      WHERE cameras.cameraId = interrogations.cameraId
                      AND nvrs.nvrId = interrogations.nvrId
                      AND users.userId = interrogations.userId
                      AND t1.cameraId = interrogations.investigatorCameraId
                      AND t2.nvrId = interrogations.investigatorNvrId
                      AND interrogations.startTime >= @start 
                      AND interrogations.endTime <= @end ) AS t1`;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .query(query);
    return result.recordset;
};

InterrogationRecord.getAmountOfInterrogationRecordsByRangeDates = async (rangeDates) => {
    const pool = await poolPromise;
    const query = `SELECT COUNT(*) as amount
                    FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2
                    WHERE cameras.cameraId = interrogations.cameraId
                    AND nvrs.nvrId = interrogations.nvrId
                    AND users.userId = interrogations.userId
                    AND t1.cameraId = interrogations.investigatorCameraId
                    AND t2.nvrId = interrogations.investigatorNvrId
                    AND interrogations.startTime >= @start 
                    AND interrogations.endTime <= @end;`;
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .query(query);
    return result.recordset[0].amount;
};

InterrogationRecord.getAmountOfInterrogationRecordsByRangeDatesAndPagination = async (rangeDates, pagination) => {
    const pool = await poolPromise;
    let query = `SELECT *
                 FROM (SELECT interrogations.interrogationId, interrogations.cameraId, interrogations.nvrId,
                            interrogations.investigatorCameraId, interrogations.investigatorNvrId, interrogations.foreignName, 
                            interrogations.caseNumber, interrogations.interviewNumberEitan, interrogations.foreignIdNumber, 
                            interrogations.nationality, interrogations.startTime, interrogations.endTime, 
                            interrogations.burned, interrogations.uploaded, cameras.cameraName, nvrs.nvrName, users.userName,
                            t1.cameraName AS 'investigatorCameraName', t2.nvrName AS 'investigatorNvrName'
                FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2
                WHERE cameras.cameraId = interrogations.cameraId
                AND nvrs.nvrId = interrogations.nvrId
                AND users.userId = interrogations.userId
                AND t1.cameraId = interrogations.investigatorCameraId
                AND t2.nvrId = interrogations.investigatorNvrId
                AND interrogations.startTime >= @start 
                AND interrogations.endTime <= @end ) AS t1`;
    query += queryHelper.getPaginationSearchQuery(pagination, searchColumns, defaultColumn);
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .query(query);
    return result.recordset.length;
};

InterrogationRecord.getInterrogationRecordsByRangeDatesAndUserIdAndConnectedUsers = async (rangeDates, userId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .input('userId', sql.Int, userId)
        .query(`SELECT interrogations.interrogationId, interrogations.cameraId, interrogations.nvrId,
                       interrogations.investigatorCameraId, interrogations.investigatorNvrId, interrogations.foreignName, 
                       interrogations.caseNumber, interrogations.interviewNumberEitan, interrogations.foreignIdNumber, 
                       interrogations.nationality, interrogations.startTime, interrogations.endTime, 
                       interrogations.burned, interrogations.uploaded, cameras.cameraName, nvrs.nvrName, users.userName,
                       t1.cameraName AS 'investigatorCameraName', t2.nvrName AS 'investigatorNvrName'
                FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2, (SELECT users.userId, users.userName 
                                                                                       FROM users 
                                                                                       WHERE users.teamLeaderId = @userId) AS t3
                WHERE cameras.cameraId = interrogations.cameraId
                AND nvrs.nvrId = interrogations.nvrId
                AND users.userId = interrogations.userId
                AND t1.cameraId = interrogations.investigatorCameraId
                AND t2.nvrId = interrogations.investigatorNvrId
                AND t3.userId = interrogations.userId
                AND interrogations.startTime >= @start 
                AND dbo.interrogations.endTime <= @end 
                UNION ALL 
                SELECT interrogations.interrogationId, interrogations.cameraId, interrogations.nvrId,
                       interrogations.investigatorCameraId, interrogations.investigatorNvrId, interrogations.foreignName, 
                       interrogations.caseNumber, interrogations.interviewNumberEitan, interrogations.foreignIdNumber, 
                       interrogations.nationality, interrogations.startTime, interrogations.endTime, 
                       interrogations.burned, interrogations.uploaded, cameras.cameraName, nvrs.nvrName, users.userName,
                       t1.cameraName AS 'investigatorCameraName', t2.nvrName AS 'investigatorNvrName'
                FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2
                WHERE cameras.cameraId = interrogations.cameraId
                AND nvrs.nvrId = interrogations.nvrId
                AND users.userId = interrogations.userId
                AND t1.cameraId = interrogations.investigatorCameraId
                AND t2.nvrId = interrogations.investigatorNvrId
                AND interrogations.userId = @userId
                AND interrogations.startTime >= @start 
                AND interrogations.endTime <= @end 
                ORDER BY interrogations.endTime DESC;`);
    return result.recordset;
};

InterrogationRecord.getInterrogationRecordsByRangeDatesAndUserId = async (rangeDates, userId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('start', sql.DateTime, rangeDates.start)
        .input('end', sql.DateTime, rangeDates.end)
        .input('userId', sql.Int, userId)
        .query(`SELECT interrogations.interrogationId, interrogations.cameraId, interrogations.nvrId,
                       interrogations.investigatorCameraId, interrogations.investigatorNvrId, interrogations.foreignName, 
                       interrogations.caseNumber, interrogations.interviewNumberEitan, interrogations.foreignIdNumber, 
                       interrogations.nationality, interrogations.startTime, interrogations.endTime, 
                       interrogations.burned, interrogations.uploaded, cameras.cameraName, nvrs.nvrName, users.userName,
                       t1.cameraName AS 'investigatorCameraName', t2.nvrName AS 'investigatorNvrName'
                FROM interrogations, cameras, nvrs, users, cameras AS t1, nvrs AS t2
                WHERE cameras.cameraId = interrogations.cameraId
                AND nvrs.nvrId = interrogations.nvrId
                AND users.userId = interrogations.userId
                AND t1.cameraId = interrogations.investigatorCameraId
                AND t2.nvrId = interrogations.investigatorNvrId
                AND interrogations.userId = @userId
                AND interrogations.startTime >= @start 
                AND interrogations.endTime <= @end 
                ORDER BY interrogations.endTime DESC;`);
    return result.recordset;
};

InterrogationRecord.getTextEditorAndChatMessagesByInterrogationId = async (interrogationId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('interrogationId', sql.Int, interrogationId)
        .query(`SELECT dbo.interrogations.editorText, dbo.interrogations.chatMessages 
                FROM dbo.interrogations 
                WHERE dbo.interrogations.interrogationId = @interrogationId;`);
    return result.recordset[0];
};

InterrogationRecord.getPauseAndContinueTimesByInterrogationId = async (interrogationId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('interrogationId', sql.Int, interrogationId)
        .query(`SELECT *
                FROM pauseInterrogations
                WHERE interrogationId = @interrogationId;`);
    return result.recordset;
};

InterrogationRecord.sendToArchive = async (interrogationId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('interrogationId', sql.Int, interrogationId)
        .query(`UPDATE interrogations  
                SET 
                    uploaded = 1 
                WHERE interrogationId = @interrogationId;`);
    return result;
};

InterrogationRecord.sendToBurn = async (interrogationId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('interrogationId', sql.Int, interrogationId)
        .query(`UPDATE interrogations  
                SET 
                    burned = 1 
                WHERE interrogationId = @interrogationId;`);
    return result;
};

InterrogationRecord.deleteInterrogationByInterrogationId = async (interrogationId) => {
    const pool = await poolPromise;
    const result = await pool.request()
        .input('interrogationId', sql.Int, interrogationId)
        .query(`DELETE FROM interrogations 
                WHERE interrogationId = @interrogationId;`);
    return result;
};

module.exports = InterrogationRecord;