const logger = require('../utils/logger');
const logService = require('../services/log');

module.exports = (action) =>
    async (req, res, next) => {
        try {
            res.on('finish', async () => {
                try {
                    if (res.statusCode === 200) {
                        await logService.createLog(req.user.userId, action);
                    }
                } catch (error) {
                    logger.error(error);
                }
            });
            next();
        } catch (error) {
            logger.error(error);
        }
    };