const Validators = require('../validators');

module.exports = (validator) => {
    //! If validator is not exist, throw err
    if (!Object.prototype.hasOwnProperty.call(Validators, validator)) {
        throw new Error(`'${validator}' validator is not exist`);
    }

    return async (req, res, next) => {
        try {
            const validated = await Validators[validator].validateAsync(req.body);
            req.body = validated;
            return next();
        } catch (err) {
            if (err.isJoi) {
                return res.status(422).send(err.message);
            }

            throw new Error(err.message);
        }
    };
};