const UserGroupTypes = require('../enums/userGroupTypes');

module.exports = (req, res, next) => {
    if (req.user.userGroupType !== UserGroupTypes.Administrator && req.user.userGroupType !== UserGroupTypes.TeamLeader) {
        return res.status(403).send('Access denied.');
    }
    return next();
};