const jwtLib = require('jsonwebtoken');
const jwt = require('../startup/jwt');

module.exports = (req, res, next) => {
    // get the token from the header that the user sent
    const token = req.header('x-auth-token');

    // the request does not have a token
    if (!token) {
        return res.status(401).send('Access denied. No token provided.');
    }

    const privateKey = jwt.getJwt();

    // verify that the digital signutare from the token is the as our enviroment variable
    req.user = jwtLib.verify(token, privateKey);
    return next();
};