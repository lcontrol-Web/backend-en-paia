module.exports = (req, res, next) => {
    if (req.user.level === 1) {
        return res.status(403).send('Access denied.');
    }
    return next();
};