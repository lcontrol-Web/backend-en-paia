const Joi = require('joi');

const logSchema = Joi.object({
    logId: Joi.number().required(),
    userId: Joi.number().required(),
    action: Joi.string().max(256).required(),
    time: Joi.date().allow(null)
});

module.exports = logSchema;