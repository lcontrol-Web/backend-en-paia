const Joi = require('joi');

const loginSchema = Joi.object({
    userName: Joi.string().min(1).max(256).required(),
    password: Joi.string().min(1).max(256).required()
});

module.exports = loginSchema;