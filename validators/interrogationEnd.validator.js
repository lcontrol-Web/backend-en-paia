const Joi = require('joi');

const interrogationEndSchema = Joi.object({
    interrogationId: Joi.number().required(),
    editorText: Joi.string().optional().allow(null),
    chatMessages: Joi.string().optional().allow(null),
    endTime: Joi.date().optional().allow(null)
});

module.exports = interrogationEndSchema;