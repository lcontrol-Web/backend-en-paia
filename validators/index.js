// Export All Schemas as Single Module
// Require all validators and export them as an object
const login = require('./login.validator');
const log = require('./log.validator');
const userGroup = require('./userGroup.validator');
const user = require('./user.validator');
const nvr = require('./nvr.validator');
const camera = require('./camera.validator');
const interrogation = require('./interrogation.validator');
const interrogationEnd = require('./interrogationEnd.validator');
const interrogationPause = require('./interrogationPause.validator');
const rangeDates = require('./rangeDates.validator');

module.exports = {
    login,
    log,
    userGroup,
    user,
    nvr,
    camera,
    interrogation,
    interrogationEnd,
    interrogationPause,
    rangeDates,
};