const Joi = require('joi');

const nvrSchema = Joi.object({
    nvrId: Joi.number().optional().allow(null),
    nvrName: Joi.string().min(1).max(256).required(),
    ip: Joi.string().ip({
        version: ['ipv4']
    }),
    channels: Joi.number().required(),
    userName: Joi.string().max(256).required(),
    password: Joi.string().required(),
    port: Joi.number().required(),
    sdkPort: Joi.number().required(),
    rtspPort: Joi.number().required()
});

module.exports = nvrSchema;