const Joi = require('joi');

const interrogationSchema = Joi.object({
    interrogationId: Joi.number().optional().allow(null),
    nvrId: Joi.number().required(),
    cameraId: Joi.number().required(),
    investigatorNvrId: Joi.number().required(),
    investigatorCameraId: Joi.number().required(),
    foreignName: Joi.string().max(256).required(),
    caseNumber: Joi.number().required(),
    interviewNumberEitan: Joi.number().required(),
    foreignIdNumber: Joi.string().max(256).required(),
    nationality: Joi.string().max(256).required(),
    userId: Joi.number().optional().allow(null),
    editorText: Joi.string().optional().allow(null),
    chatMessages: Joi.string().optional().allow(null),
    startTime: Joi.date().optional().allow(null),
    endTime: Joi.date().optional().allow(null),
    burned: Joi.boolean().optional().allow(null),
    uploaded: Joi.boolean().optional().allow(null),
});

module.exports = interrogationSchema;