const Joi = require('joi');

const cameraSchema = Joi.object({
    nvrId: Joi.number().required(),
    cameraId: Joi.number().optional().allow(null),
    cameraName: Joi.string().max(256).required(),
    channel: Joi.number().required(),
    cameraIp: Joi.string().ip({
        version: ['ipv4']
    }),
    controllerIp: Joi.string().ip({
        version: ['ipv4']
    }),
    indexArray: Joi.number().required()
});

module.exports = cameraSchema;