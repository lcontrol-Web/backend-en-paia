const Joi = require('joi');

const userSchema = Joi.object({
    userId: Joi.number().optional().allow(null),
    userName: Joi.string().min(1).max(256).required(),
    userGroupId: Joi.number().required(),
    teamLeaderId: Joi.number().optional().allow(null)
});

module.exports = userSchema;