const Joi = require('joi');

const userGroupSchema = Joi.object({
    userGroupId: Joi.number().optional().allow(null),
    userGroupName: Joi.string().min(1).max(256).required(),
    userGroupType: Joi.string().min(1).max(256).required()
});

module.exports = userGroupSchema;