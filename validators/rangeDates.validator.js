const Joi = require('joi');

const rangeDatesSchema = Joi.array().items(Joi.date().required());

module.exports = rangeDatesSchema;