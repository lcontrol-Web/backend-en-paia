const LIVE_INTERROGATION_INTERVAL = 5000;

const clients = new Set();

function getLiveInterrogations() {
    const liveInterrogations = [];

    clients.forEach((client) => {
        if (+client.handshake.query.clientType === 2) {
            liveInterrogations.push(client.liveInterrogation);
        }
    });

    return liveInterrogations;
}

function getAdmins() {
    const admins = [];
    clients.forEach((client) => {
        if (+client.handshake.query.clientType === 1) {
            admins.push(client);
        }
    });

    return admins;
}

function disconnectSocket(socketId) {
    const client = clients.find((c) =>
        c.id === socketId);

    if (client) {
        client.disconnect();
    }
}

function sendLiveInterrogationsToAdminsInterval() {
    const admins = getAdmins();
    const liveInterrogations = getLiveInterrogations();

    admins.forEach((admin) => {
        admin.emit('live-interrogations', liveInterrogations);
    });
}

function startLiveInterrogationsToAdminsInterval() {
    setInterval(() => {
        sendLiveInterrogationsToAdminsInterval();
    }, LIVE_INTERROGATION_INTERVAL);
}

// connection types:
// 2-> someone started an interrogation
module.exports = (io) => {
    startLiveInterrogationsToAdminsInterval();

    io.on('connection', (client) => {
        clients.add(client);
        console.log(`Socket ${client.id} added`);
        console.log(`Total clients: ${clients.size}`);

        // create a room with the client id of the live interrogation
        client.on('create-room', (data) => {
            client.join(client.id);
            client.liveInterrogation = data;
            console.log(`Created a room ${client.id}`);
            client.emit('connected-to-room', client.id);
            sendLiveInterrogationsToAdminsInterval();
        });

        client.on('connect-to-room', (socketId) => {
            client.join(socketId);
            client.emit('connected-to-room', socketId);
        });

        client.on('send-message', (message, callback) => {
            try {
                io.sockets.in(message.to).emit('message', message);
            } catch (error) {
                callback(error, null);
            }
        });

        client.on('disconnect', () => {
            clients.delete(client);
            console.log(`Remaining clients: ${clients.size}`);
            sendLiveInterrogationsToAdminsInterval();
        });
    });
};

module.exports.disconnectSocket = disconnectSocket;