const express = require('express');
const logs = require('../routes/logs');
const auth = require('../routes/auth');
const nvrs = require('../routes/nvrs');
const users = require('../routes/users');
const stream = require('../routes/stream');
const cameras = require('../routes/cameras');
const userGroups = require('../routes/userGroups');
const controller = require('../routes/controller');
const interrogations = require('../routes/interrogations');
const interrogationRecords = require('../routes/interrogationRecords');

// for error handling
const error = require('../middleware/error');

module.exports = (app) => {
    app.use(express.json());
    app.use('/api/auth', auth);
    app.use('/api/user-groups', userGroups);
    app.use('/api/nvrs', nvrs);
    app.use('/api/logs', logs);
    app.use('/api/users', users);
    app.use('/api/stream', stream);
    app.use('/api/cameras', cameras);
    app.use('/api/controller', controller);
    app.use('/api/interrogations', interrogations);
    app.use('/api/interrogation-records', interrogationRecords);

    // this middleware error function will activate after the routes above, by calling the next() function in the routes
    app.use(error);
};