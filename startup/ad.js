const ActiveDirectory = require('activedirectory');
const schedule = require('node-schedule');
const logger = require('../utils/logger');
const {
    poolPromise
} = require('../db');
const User = require('../models/user');

let ad = {};

async function setAd() {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT adUrl, adBaseDn, adUser, adPass   
                FROM config;`);

    ad = new ActiveDirectory({
        url: result.recordset[0].adUrl,
        baseDN: result.recordset[0].adBaseDn,
        username: result.recordset[0].adUser,
        password: result.recordset[0].adPass
    });
}

function getAd() {
    return ad;
}

async function getAdGroupName() {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT adGroup 
                FROM config;`);

    return result.recordset[0].adGroup;
}

async function checkAdUsers() {
    logger.info('checking for new users in active directory');
    const adGroupName = await getAdGroupName();
    ad.getUsersForGroup(adGroupName, async (err, users) => {
        if (err) {
            logger.error(err.message, err);
            return;
        }

        const dbUsers = await User.getUsers();

        // if (users) {
        //     for (const user of users) {
        //         const found = dbUsers.some((dbUser) =>
        //             dbUser.userName.toLowerCase() === user.sAMAccountName.toLowerCase());
        //         if (!found) {
        //             await User.addUser({
        //                 userName: user.sAMAccountName.toLowerCase(),
        //                 userGroupId: process.env.NO_PERMISSION_ID,
        //                 teamLeaderId: null
        //             });
        //         }
        //     }
        // }
    });
}

function startAdUsersTimer() {
    checkAdUsers();

    // const job = schedule.scheduleJob('*/10 * * * * *', function (fireDate) {

    // call a function each hour
    const job = schedule.scheduleJob('* 1 * * *', (fireDate) => {
        checkAdUsers();
    });
}

function isUserMemberOfGroup(userName) {
    return new Promise(async (resolve, reject) => {
        const adGroupName = await ActiveDirectory.getAdGroupName();
        ad.isUserMemberOf(userName, adGroupName, (err, isMember) => {
            if (err) {
                logger.error(err.message, err);
                reject(err);
            } else { resolve(isMember); }
        });
    });
}

function getAdUserByUserName(userName) {
    return new Promise((resolve, reject) => {
        ad.findUser(userName, (err, user) => {
            if (err) {
                logger.error(err.message, err);
                reject(err);
            } else { resolve(user); }
        });
    });
}

module.exports.getAd = getAd;
module.exports.setAd = setAd;
module.exports.startAdUsersTimer = startAdUsersTimer;
module.exports.getAdGroupName = getAdGroupName;
module.exports.getAdUserByUserName = getAdUserByUserName;
module.exports.isUserMemberOfGroup = isUserMemberOfGroup;