// const sql = require('mssql/msnodesqlv8');
const {
    poolPromise
} = require('../db');

let jwt;

module.exports = async () => {
    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT jwt 
                FROM config;`);

    jwt = result.recordset[0].jwt;
};

function getJwt() {
    return jwt;
}

module.exports.getJwt = getJwt;