const path = require('path');
const {
    createLogger,
    transports,
    format
} = require('winston');

const logger = createLogger({
    transports: [
        new transports.File({
            filename: path.join(__dirname, '../logs/info.log'),
            level: 'info',
            maxsize: 10000000,
            maxFiles: 20,
            format: format.combine(format.timestamp(), format.json()),
        }),
        new transports.File({
            filename: path.join(__dirname, '../logs/error.log'),
            level: 'error',
            maxsize: 10000000,
            maxFiles: 20,
            handleRejections: true,
            handleExceptions: true,
            format: format.combine(format.timestamp(), format.json()),
        }),
    ],
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(
        new transports.Console({
            level: 'info',
            handleRejections: true,
            handleExceptions: true,
            format: format.combine(format.colorize(), format.simple()),
        })
    );
}

module.exports = logger;