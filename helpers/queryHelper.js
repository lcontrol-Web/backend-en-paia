const SortTypes = require('../enums/sortTypes');

function getFilterType(filterType, searchText) {
    switch (filterType) {
        // contains
        case 1: {
            return ` LIKE ${`'%${searchText}%'`} `;
        }
        // not contains
        case 2: {
            return ` NOT LIKE ${`'%${searchText}%'`}`;
        }
        // starts with
        case 3: {
            return ` LIKE ${`'${searchText}%'`} `;
        }
        // ends with
        case 4: {
            return ` LIKE ${`'%${searchText}'`} `;
        }
        // equal
        case 5: {
            return ` = ${searchText} `;
        }
        default:
            return ` LIKE ${`'%${searchText}%'`} `;
    }
}

function getSearchQuery(pagination, defaultSearchColumns) {
    let query = '';
    if (pagination.searchText) {
        query += ' WHERE (';

        if (pagination.column) {
            query += ` ${pagination.column} ${getFilterType(pagination.condition, pagination.searchText)})`;
        } else {
            defaultSearchColumns.forEach((defaultColumn) => {
                query += ` ${defaultColumn} LIKE ${`'%${pagination.searchText}%'`} OR`;
            });
            const lastIndex = query.lastIndexOf('OR');
            query = query.slice(0, lastIndex) + query.slice(lastIndex).replace('OR', ')');
        }
    }

    return query;
}

function getOrderQuery(column, sort, defaultColumn) {
    let query = ' ORDER BY ';

    if (!column && sort === SortTypes.Ascending) {
        query += `${defaultColumn} ASC `;
    } else if (!column && sort === SortTypes.Descending) {
        query += `${defaultColumn} DESC `;
    } else if (column && sort === SortTypes.Ascending) {
        query += `${column} ASC `;
    } else if (column && sort === SortTypes.Descending) {
        query += `${column} DESC `;
    }

    return query;
}

function getlimitAndOffsetQuery(limit, offset) {
    return `OFFSET (${offset}) ROWS FETCH NEXT (${limit}) ROWS ONLY; `;
}

function getPaginationSearchQuery(pagination, defaultSearchColumns, defaultColumn) {
    let query = '';
    query += getSearchQuery(pagination, defaultSearchColumns);
    query += getOrderQuery(pagination.column, pagination.sort, defaultColumn);
    query += getlimitAndOffsetQuery(pagination.limit, pagination.offset);
    return query;
}

module.exports.getPaginationSearchQuery = getPaginationSearchQuery;
module.exports.getSearchQuery = getSearchQuery;