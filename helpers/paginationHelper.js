const Pagination = require('../models/pagination');

function getPagination(limit, page, sort, searchText, column, condition) {
    const pagination = new Pagination({});
    pagination.limit = +limit || 10;
    pagination.page = +page || 1;
    pagination.offset = (+page - 1) * limit;
    pagination.sort = sort || 'desc';
    pagination.searchText = searchText || null;
    pagination.column = column || null;
    pagination.condition = +condition || null;
    return pagination;
}

module.exports.getPagination = getPagination;