const interrogationRecordService = require('../services/interrogationRecord');
const paginationHelper = require('../helpers/paginationHelper');
const userGroupTypes = require('../enums/userGroupTypes');

class RangeDates {
    constructor(start, end) {
        this.start = new Date(start);
        this.end = new Date(end);
    }
}

const getInterrogationRecordsByRangeDates = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    const rangeDates = new RangeDates(new Date(req.body[0]), new Date(req.body[1]));
    const {
        userGroupType,
        userId
    } = req.user;
    let interrogationRecords = [];

    switch (userGroupType) {
        case userGroupTypes.Administrator:
        case userGroupTypes.Manager: {
            if (!limit) {
                interrogationRecords = await interrogationRecordService.getInterrogationRecordsByRangeDates(rangeDates);
            } else {
                const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
                interrogationRecords = await interrogationRecordService.getInterrogationRecordsByRangeDatesAndPagination(rangeDates, pagination);
            }
            break;
        }
        case userGroupTypes.TeamLeader:
            interrogationRecords = await interrogationRecordService.getInterrogationRecordsByRangeDatesAndUserIdAndConnectedUsers(rangeDates, userId);
            break;
        case userGroupTypes.Investigator:
            interrogationRecords = await interrogationRecordService.getInterrogationRecordsByRangeDatesAndUserId(rangeDates, userId);
            break;
        default:
            break;
    }

    res.send(interrogationRecords);
};

const getAmountOfInterrogationRecordsByRangeDates = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    const rangeDates = new RangeDates(new Date(req.body[0]), new Date(req.body[1]));

    if (!limit) {
        const amount = await interrogationRecordService.getAmountOfInterrogationRecordsByRangeDates(rangeDates);
        res.send(amount.toString());
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const amount = await interrogationRecordService.getAmountOfInterrogationRecordsByRangeDatesAndPagination(rangeDates, pagination);
        res.send(amount.toString());
    }
};

const getTextEditorAndChatMessagesByInterrogationId = async (req, res, next) => {
    const interrogationId = +req.params.id;
    const interrogations = await interrogationRecordService.getTextEditorAndChatMessagesByInterrogationId(interrogationId);
    res.send(interrogations);
};

const getPauseAndContinueTimesByInterrogationId = async (req, res, next) => {
    const interrogationId = +req.params.id;
    const interrogations = await interrogationRecordService.getPauseAndContinueTimesByInterrogationId(interrogationId);
    res.send(interrogations);
};

const sendToArchive = async (req, res, next) => {
    const interrogationId = +req.params.id;
    const result = await interrogationRecordService.sendToArchive(interrogationId);
    res.send(result);
};

const sendToBurn = async (req, res, next) => {
    const interrogationId = +req.params.id;
    const result = await interrogationRecordService.sendToBurn(interrogationId);
    res.send(result);
};

module.exports = {
    getInterrogationRecordsByRangeDates,
    getAmountOfInterrogationRecordsByRangeDates,
    getTextEditorAndChatMessagesByInterrogationId,
    getPauseAndContinueTimesByInterrogationId,
    sendToArchive,
    sendToBurn,

};