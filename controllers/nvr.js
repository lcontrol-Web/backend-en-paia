const nvrService = require('../services/nvr');
const cameraService = require('../services/camera');
const paginationHelper = require('../helpers/paginationHelper');

const getNvrs = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    if (!limit) {
        const nvrs = await nvrService.getNvrs();
        res.send(nvrs);
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const nvrs = await nvrService.getNvrsByPagination(pagination);
        res.send(nvrs);
    }
};

const getNvrByNvrId = async (req, res, next) => {
    const nvr = await nvrService.getNvrByNvrId(+req.params.id);
    if (!nvr) {
        return res.status(404).send('The nvr with the given ID not found');
    }

    return res.send(nvr);
};

const getCamerasByNvrId = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    const nvrId = +req.params.id;

    if (!limit) {
        const users = await cameraService.getCamerasByNvrId(nvrId);
        res.send(users);
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const users = await cameraService.getCamerasByNvrIdAndPagination(nvrId, pagination);
        res.send(users);
    }
};

const getAmountOfNvrs = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    if (!searchText) {
        const amount = await nvrService.getAmountOfNvrs();
        res.send(amount.toString());
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const amount = await nvrService.getAmountOfNvrsByPagination(pagination);
        res.send(amount.toString());
    }
};

const createNvr = async (req, res, next) => {
    const newNvr = await nvrService.createNvr(req.body);
    res.send(newNvr);
};

const updateNvr = async (req, res, next) => {
    const nvrId = +req.params.id;
    const nvr = await nvrService.getNvrByNvrId(nvrId);
    if (!nvr) {
        return res.status(404).send('The nvr with the given ID not found');
    }

    const updatedNvr = await nvrService.updateNvr(nvrId, req.body);
    return res.send(updatedNvr);
};

const deleteNvr = async (req, res, next) => {
    const nvrId = +req.params.id;
    const nvr = await nvrService.getNvrByNvrId(nvrId);
    if (!nvr) {
        return res.status(404).send('The nvr with the given ID not found');
    }

    await nvrService.deleteNvr(nvrId);
    return res.send(nvrId);
};

const deleteNvrs = async (req, res, next) => {
    const nvrIds = req.body;
    await nvrService.deleteNvrs(nvrIds);
    return res.send(nvrIds);
};

module.exports = {
    getNvrs,
    getNvrByNvrId,
    getCamerasByNvrId,
    getAmountOfNvrs,
    createNvr,
    updateNvr,
    deleteNvr,
    deleteNvrs,
};