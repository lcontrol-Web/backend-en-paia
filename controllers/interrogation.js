const interrogationService = require('../services/interrogation');
const socketService = require('../services/socket');
const paginationHelper = require('../helpers/paginationHelper');

const getInterrogations = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    if (!limit) {
        const interrogations = await interrogationService.getInterrogations();
        res.send(interrogations);
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const interrogations = await interrogationService.getInterrogationsByPagination(pagination);
        res.send(interrogations);
    }
};

const getOpenInterrogations = async (req, res, next) => {
    const interrogations = await interrogationService.getOpenInterrogations();
    res.send(interrogations);
};

const getInterrogationByInterrogationId = async (req, res, next) => {
    const interrogation = await interrogationService.getInterrogationByInterrogationId(+req.params.id);
    if (!interrogation) {
        return res.status(404).send('The interrogation with the given ID not found');
    }

    return res.send(interrogation);
};

const closeActiveInterrogation = async (req, res, next) => {
    const {
        socketId
    } = req.params;

    if (socketId) {
        return res.status(400).send('socket id is required');
    }

    const status = socketService.disconnect(socketId);
    return res.send({
        status
    });
};

const startInterrogation = async (req, res, next) => {
    const userId = +req.user.userId;
    const interrogation = await interrogationService.startInterrogation(req.body, userId);
    res.send(interrogation);
};

const endInterrogation = async (req, res, next) => {
    const interrogationId = +req.params.id;
    const interrogation = await interrogationService.getInterrogationByInterrogationId(interrogationId);
    if (!interrogation) {
        return res.status(400).send('interrogation not exist');
    }

    await interrogationService.endInterrogation(interrogationId, req.body);
    return res.send(req.body);
};

const pauseInterrogation = async (req, res, next) => {
    const interrogationId = +req.params.id;
    const interrogation = await interrogationService.getInterrogationByInterrogationId(interrogationId);
    if (!interrogation) {
        return res.status(400).send('interrogation not exist');
    }

    await interrogationService.pauseInterrogation(interrogationId, req.body);
    return res.send(req.body);
};

const continueInterrogation = async (req, res, next) => {
    const interrogationId = +req.params.id;
    const interrogation = await interrogationService.getInterrogationByInterrogationId(interrogationId);
    if (!interrogation) {
        return res.status(400).send('interrogation not exist');
    }

    await interrogationService.continueInterrogation(interrogationId);
    return res.send();
};

const deleteInterrogation = async (req, res, next) => {
    const interrogationId = +req.params.id;
    const interrogation = await interrogationService.getInterrogationByInterrogationId(interrogationId);
    if (!interrogation) {
        return res.status(404).send('The interrogation with the given ID not found');
    }

    await interrogationService.deleteInterrogation(interrogationId);
    return res.send(interrogationId);
};

const deleteInterrogations = async (req, res, next) => {
    const interrogationIds = req.body;
    await interrogationService.deleteInterrogations(interrogationIds);
    return res.send(interrogationIds);
};

module.exports = {
    getInterrogations,
    getOpenInterrogations,
    getInterrogationByInterrogationId,
    closeActiveInterrogation,
    startInterrogation,
    endInterrogation,
    pauseInterrogation,
    continueInterrogation,
    deleteInterrogation,
    deleteInterrogations,
};