const userService = require('../services/user');
const userGroupService = require('../services/userGroup');
const userGroupTypes = require('../enums/userGroupTypes');
const paginationHelper = require('../helpers/paginationHelper');

const getUserGroups = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    if (!limit) {
        const userGroups = await userGroupService.getUserGroups();
        res.send(userGroups);
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const userGroups = await userGroupService.getUserGroupsByPagination(pagination);
        res.send(userGroups);
    }
};

const getUserGroupByUserGroupId = async (req, res, next) => {
    const userGroup = await userGroupService.getUserGroupByUserGroupId(+req.params.id);
    if (!userGroup) {
        return res.status(404).send('The user group with the given ID not found');
    }

    return res.send(userGroup);
};

const getUsersByUserGroupId = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    const {
        userGroupType,
        userId
    } = req.user;

    const userGroupId = +req.params.id;

    switch (userGroupType) {
        case userGroupTypes.Administrator: {
            if (!limit) {
                const users = await userService.getUsersByUserGroupId(userGroupId);
                res.send(users);
            } else {
                const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
                const users = await userService.getUsersByUserGroupIdAndPagination(userGroupId, pagination);
                res.send(users);
            }

            break;
        }
        case userGroupTypes.TeamLeader: {
            if (!limit) {
                const users = await userService.getUsersByUserGroupIdAndTeamLeaderId(userGroupId, userId);
                res.send(users);
            } else {
                const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
                const users = await userService.getUsersByUserGroupIdAndTeamLeaderIdAndPagination(userGroupId, userId, pagination);
                res.send(users);
            }

            break;
        }
        default:
            res.send([]);
    }
};

const getAmountOfUserGroups = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    if (!searchText) {
        const amount = await userGroupService.getAmountOfUserGroups();
        res.send(amount.toString());
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const amount = await userGroupService.getAmountOfUserGroupsByPagination(pagination);
        res.send(amount.toString());
    }
};

const createUserGroup = async (req, res, next) => {
    const newUserGroup = await userGroupService.createUserGroup(req.body);
    res.send(newUserGroup);
};

const updateUserGroup = async (req, res, next) => {
    const userGroupId = +req.params.id;
    const userGroup = await userGroupService.getUserGroupByUserGroupId(userGroupId);
    if (!userGroup) {
        return res.status(404).send('The user group with the given ID not found');
    }

    const updatedUserGroup = await userGroupService.updateUserGroup(userGroupId, req.body);
    return res.send(updatedUserGroup);
};

const deleteUserGroup = async (req, res, next) => {
    const userGroupId = +req.params.id;
    const userGroup = await userGroupService.getUserGroupByUserGroupId(userGroupId);
    if (!userGroup) {
        return res.status(404).send('The user group with the given ID not found');
    }

    await userGroupService.deleteUserGroup(userGroupId);
    return res.send(userGroupId);
};

const deleteUserGroups = async (req, res, next) => {
    const userGroupIds = req.body;
    await userGroupService.deleteUserGroups(userGroupIds);
    return res.send(userGroupIds);
};

module.exports = {
    getUserGroups,
    getUserGroupByUserGroupId,
    getUsersByUserGroupId,
    getAmountOfUserGroups,
    createUserGroup,
    updateUserGroup,
    deleteUserGroup,
    deleteUserGroups,
};