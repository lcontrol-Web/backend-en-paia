const logService = require('../services/log');
const UserGroupTypes = require('../enums/userGroupTypes');
const paginationHelper = require('../helpers/paginationHelper');

class RangeDates {
    constructor(start, end) {
        this.start = new Date(start);
        this.end = new Date(end);
    }
}

const getLogsByRangeDates = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    const rangeDates = new RangeDates(new Date(req.body[0]), new Date(req.body[1]));
    const {
        userGroupType,
        userId
    } = req.user;

    let logs = [];

    switch (userGroupType) {
        case UserGroupTypes.Administrator:
            logs = await logService.getLogsByRangeDates(rangeDates);
            break;
        case UserGroupTypes.TeamLeader:
            logs = await logService.getTeamLeaderLogsByRangeDates(rangeDates, userId);
            break;
        default:
            break;
    }

    // switch (userGroupType) {
    //     case UserGroupTypes.Administrator:
    //         if (!limit) {
    //             logs = await logService.getLogsByRangeDates(rangeDates);
    //         } else {
    //             const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
    //             logs = await logService.getLogsByRangeDatesAndPagination(rangeDates, pagination);
    //         }
    //         break;
    //     case UserGroupTypes.TeamLeader:
    //         if (!limit) {
    //             logs = await logService.getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDates(userId, rangeDates);
    //         } else {
    //             const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
    //             logs = await logService.getTeamLeaderAndInvestigatorsLogsByTeamLeaderIdAndRangeDatesPagination(userId, rangeDates, pagination);
    //         }
    //         break;
    //     default:
    //         break;
    // }

    res.send(logs);

    // if (!limit) {
    //     const logs = await logService.getLogsByRangeDates(rangeDates);
    //     res.send(logs);
    // } else {
    //     const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
    //     const logs = await logService.getLogsByRangeDatesAndPagination(rangeDates, pagination);
    //     res.send(logs);
    // }
};

const getAmountOfLogsByRangeDates = async (req, res, next) => {
    const nvrId = +req.params.nvrId;
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    const rangeDates = new RangeDates(new Date(req.body[0]), new Date(req.body[1]));

    if (!searchText) {
        const amount = await logService.getAmountOfLogsByRangeDates(rangeDates);
        res.send(amount.toString());
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const amount = await logService.getAmountOfLogsByRangeDatesAndPagination(rangeDates, pagination);
        res.send(amount.toString());
    }
};

module.exports = {
    getLogsByRangeDates,
    getAmountOfLogsByRangeDates,
};