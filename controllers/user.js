const userService = require('../services/user');
const userGroupTypes = require('../enums/userGroupTypes');
const paginationHelper = require('../helpers/paginationHelper');

const getUsers = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    const {
        userGroupType,
        userId
    } = req.user;

    switch (userGroupType) {
        case userGroupTypes.Administrator: {
            if (!limit) {
                const users = await userService.getUsers();
                res.send(users);
            } else {
                const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
                const users = await userService.getUsersByPagination(pagination);
                res.send(users);
            }

            break;
        }
        case userGroupTypes.TeamLeader: {
            if (!limit) {
                const users = await userService.getUsersByTeamLeaderId(userId);
                res.send(users);
            } else {
                const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
                const users = await userService.getUsersByTeamLeaderIdAndPagination(pagination);
                res.send(users);
            }

            break;
        }
        default:
            res.send([]);
    }
};

const getTeamLeaders = async (req, res, next) => {
    const user = await userService.getTeamLeaders();
    return res.send(user);
};

const getUserByUserId = async (req, res, next) => {
    const user = await userService.getUserByUserId(+req.params.id);
    if (!user) {
        return res.status(404).send('The user with the given ID not found');
    }

    return res.send(user);
};

const getAmountOfUsersByUserGroupId = async (req, res, next) => {
    const userGroupId = +req.params.userGroupId;

    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    const {
        userGroupType,
        userId
    } = req.user;

    switch (userGroupType) {
        case userGroupTypes.Administrator: {
            if (!searchText) {
                const amount = await userService.getAmountOfUsersByUserGroupId(userGroupId);
                res.send(amount.toString());
            } else {
                const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
                const amount = await userService.getAmountOfUsersByUserGroupIdAndPagination(userGroupId, pagination);
                res.send(amount.toString());
            }
            break;
        }
        case userGroupTypes.TeamLeader: {
            if (!searchText) {
                const amount = await userService.getAmountOfUsersByUserGroupIdAndTeamLeaderId(userGroupId, userId);
                res.send(amount.toString());
            } else {
                const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
                const amount = await userService.getAmountOfUsersByUserGroupIdAndTeamLeaderIdAndPagination(userGroupId, userId, pagination);
                res.send(amount.toString());
            }
            break;
        }
        default:
            res.send([]);
    }
};

const createUser = async (req, res, next) => {
    const exists = await userService.getUserByUserName(req.body.userName);
    if (exists) {
        return res.status(409).send('User name exists');
    }

    const user = await userService.createUser(req.body);
    return res.send(user);
};

const updateUser = async (req, res, next) => {
    const userId = +req.params.id;
    let user = await userService.getUserByUserId(userId);
    if (!user) {
        return res.status(404).send('The user with the given ID not found');
    }

    // TO DO - check if user name already exists
    user = await userService.getUserByUserName(req.body.userName);
    if (user && user.userId !== userId) {
        return res.status(409).send('User name exists');
    }

    const updatedUser = await userService.updateUser(userId, req.body);
    return res.send(updatedUser);
};

const deleteUser = async (req, res, next) => {
    const userId = +req.params.id;
    const user = await userService.getUserByUserId(userId);
    if (!user) {
        return res.status(404).send('The user with the given ID not found');
    }

    await userService.deleteUser(userId);
    return res.send(userId);
};

const deleteUsers = async (req, res, next) => {
    const userIds = req.body;
    await userService.deleteUsers(userIds);
    return res.send(userIds);
};

module.exports = {
    getUsers,
    getTeamLeaders,
    getUserByUserId,
    getAmountOfUsersByUserGroupId,
    createUser,
    updateUser,
    deleteUser,
    deleteUsers,
};