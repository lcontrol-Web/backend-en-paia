/* eslint-disable consistent-return */
const fs = require('fs');
const jwtLib = require('jsonwebtoken');
const jwt = require('../startup/jwt');
const streamService = require('../services/stream');
const logger = require('../utils/logger');
const {
    sql,
    poolPromise
} = require('../db');

function checkAuth(token) {
    const privateKey = jwt.getJwt();
    const user = jwtLib.verify(token, privateKey);
}

const startStreaming = async (req, res, next) => {
    const {
        token,
        file,
        folder
    } = req.query;

    if (!token || !file || !folder) {
        return res.status(400).send('not enought information');
    }

    checkAuth(token);

    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT sharedFolder
            FROM config;`);
    const {
        sharedFolder
    } = result.recordset[0];
    const path = `${sharedFolder}${folder}\\${file}`;

    // const path = 'C:\\Development\\paia-en\\backend\\video2.mp4';
    const stat = fs.statSync(path);
    const fileSize = stat.size;
    const {
        range
    } = req.headers;
    if (range) {
        const parts = range.replace(/bytes=/, '').split('-');
        const start = parseInt(parts[0], 10);
        const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
        const chunksize = (end - start) + 1;
        const file2 = fs.createReadStream(path, {
            start,
            end
        });

        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': 'video/mp4',
        };
        res.writeHead(206, head);
        file2.pipe(res);
    } else {
        const head = {
            'Content-Length': fileSize,
            'Content-Type': 'video/mp4',
        };
        res.writeHead(200, head);
        fs.createReadStream(path).pipe(res);
    }
};

const getFilesInsideFolderByInterrogationId = async (req, res, next) => {
    const interrogationId = +req.params.id;
    if (!interrogationId) {
        return res.status(400).send('interrogation id is required');
    }

    const pool = await poolPromise;
    const result = await pool.request()
        .query(`SELECT sharedFolder 
                FROM config;`);

    const {
        sharedFolder
    } = result.recordset[0];
    const fullPath = sharedFolder + interrogationId;
    const dirFiles = [];

    try {
        fs.readdir(fullPath, (err, files) => {
            if (err && err.message.includes('no such file or directory')) {
                return res.status(400).send('directoryNotExist');
            }
            if (err) {
                logger.error(err.message, err);
                return res.status(400).send('cannot read files from shared directory');
            }

            files.forEach((file) => {
                if (file.endsWith('mp4')) {
                    dirFiles.push(file);
                }
            });

            res.send(dirFiles);
        });
    } catch (err) {
        logger.error(err.message, err);
        return res.status(400).send('cannot read files from directory');
    }
};

module.exports = {
    startStreaming,
    getFilesInsideFolderByInterrogationId
};