const cameraService = require('../services/camera');
const paginationHelper = require('../helpers/paginationHelper');

const getCameras = async (req, res, next) => {
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    if (!limit) {
        const cameras = await cameraService.getCameras();
        res.send(cameras);
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const cameras = await cameraService.getCamerasByPagination(pagination);
        res.send(cameras);
    }
};

const getCameraByCameraId = async (req, res, next) => {
    const camera = await cameraService.getCameraByCameraId(+req.params.id);
    if (!camera) {
        return res.status(404).send('The camera with the given ID not found');
    }

    return res.send(camera);
};

const getAmountOfCamerasByNvrId = async (req, res, next) => {
    const nvrId = +req.params.nvrId;
    const {
        limit,
        page,
        sort,
        column,
        searchText,
        condition
    } = req.query;

    if (!searchText) {
        const amount = await cameraService.getAmountOfCamerasByNvrId(nvrId);
        res.send(amount.toString());
    } else {
        const pagination = paginationHelper.getPagination(limit, page, sort, searchText, column, condition);
        const amount = await cameraService.getAmountOfCamerasByNvrIdAndPagination(nvrId, pagination);
        res.send(amount.toString());
    }
};

const createCamera = async (req, res, next) => {
    const newCamera = await cameraService.createCamera(req.body);
    res.send(newCamera);
};

const updateCamera = async (req, res, next) => {
    const cameraId = +req.params.id;
    const camera = await cameraService.getCameraByCameraId(cameraId);
    if (!camera) {
        return res.status(404).send('The camera with the given ID not found');
    }

    const updatedCamera = await cameraService.updateCamera(cameraId, req.body);
    return res.send(updatedCamera);
};

const deleteCamera = async (req, res, next) => {
    const cameraId = +req.params.id;
    const camera = await cameraService.getCameraByCameraId(cameraId);
    if (!camera) {
        return res.status(404).send('The camera with the given ID not found');
    }

    await cameraService.deleteCamera(cameraId);
    return res.send(cameraId);
};

const deleteCameras = async (req, res, next) => {
    const cameraIds = req.body;
    await cameraService.deleteCameras(cameraIds);
    return res.send(cameraIds);
};

module.exports = {
    getCameras,
    getCameraByCameraId,
    getAmountOfCamerasByNvrId,
    createCamera,
    updateCamera,
    deleteCamera,
    deleteCameras,
};