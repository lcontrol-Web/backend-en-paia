const ModbusRTU = require('modbus-serial');
const ping = require('ping');
const cameraService = require('../services/camera');

const pingIp = async (req, res, next) => {
    const {
        ip
    } = req.params;

    ping.sys.probe(ip, (isAlive) => {
        res.send(isAlive);
    });
};

const getOutputStatusByCameraId = async (req, res, next) => {
    const cameraId = +req.params.cameraId;
    const camera = await cameraService.getCameraByCameraId(cameraId);
    const client = new ModbusRTU();

    try {
        await client.connectTCP(camera.controllerIp, {
            port: 502
        });

        client.readCoils(camera.indexArray - 1, 2)
            .then((result) => {
                client.close();
                return res.status(200).send(result.data[0]);
            })
            .catch((err) => {
                client.close();
                return res.status(500).send(JSON.stringify('Cannot read output status'));
            });
    } catch (error) {
        client.close();
        return res.status(500).send(JSON.stringify('Cannot connect to controller'));
    }
};

const turnOnOffOutput = async (req, res, next) => {
    const cameraId = +req.params.cameraId;
    const camera = await cameraService.getCameraByCameraId(cameraId);
    const data = req.body;
    const client = new ModbusRTU();

    try {
        await client.connectTCP(camera.controllerIp, {
            port: 502
        });

        await client.writeCoils(camera.indexArray - 1, [data.active, data.active])
            .then(async (result) => {
                await client.readCoils(camera.indexArray - 1, 2)
                    .then((result1) => {
                        client.close();
                        return res.status(200).send(result1.data[0]);
                    })
                    .catch((err) => {
                        client.close();
                        return res.status(500).send(JSON.stringify('Cannot read output status'));
                    });
            })
            .catch((err) => {
                client.close();
                return res.status(500).send(JSON.stringify('Cannot active output'));
            });
    } catch (error) {
        client.close();
        return res.status(500).send(JSON.stringify('Cannot connect to controller'));
    }
};

module.exports = {
    getOutputStatusByCameraId,
    turnOnOffOutput,
    pingIp,
};