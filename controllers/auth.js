const activeDirectory = require('../startup/ad');
const userService = require('../services/user');

const authenticateUser = async (req, res, next) => {
    const user = await userService.getUserByUserNameForAuth(req.body.userName);
    if (!user) {
        return res.status(400).send('invalid user name or password');
    }

    const token = userService.generateToken(user);
    res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');

    // for writing to logger
    req.user = user;

    return res.set('x-auth-token', token).send(user);

    // const ad = activeDirectory.getAd();
    // const adGroupName = await activeDirectory.getAdGroupName();
    // ad.isUserMemberOf(loggedUser.userName, adGroupName, (err, isMember) => {

    //     if (err) {
    //         logger.error(err.message, err);
    //         return res.status(400).send('user is not member of the group');
    //     }

    //     if (!isMember)
    //         return res.status(400).send('user is not member of the group');

    //     ad.findUser(loggedUser.userName, (err, user) => {
    //         if (err) {
    //             logger.error(err.message, err);
    //             return res.status(400).send('cannot find user in active directory');
    //         }

    //         if (!user || !user.userPrincipalName)
    //             return res.status(400).send('cannot find user in active directory');

    //         // @123456789#!
    //         let userId = '@' + user.userPrincipalName.substring(0, user.userPrincipalName.indexOf('@')) + '#!';

    //         // check password againts the id of the user
    //         if (userId != req.body.password)
    //             return res.status(400).send('Invalid user name or password');

    //         const token = loggedUser.generateAuthToken();
    //         res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');

    //         // add to the request the logged user for saving in the logs table
    //         req.user = loggedUser;

    //         res.set('x-auth-token', token).send(loggedUser);

    //     });

    // });
};

module.exports = {
    authenticateUser,
};