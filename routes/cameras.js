const express = require('express');
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');
const actionLogger = require('../middleware/logger');
const administrator = require('../middleware/administrator');
const ValidatorEnum = require('../enums/validatorEnum');
const LoggerActions = require('../enums/loggerActions');

const {
    getCameras,
    getCameraByCameraId,
    getAmountOfCamerasByNvrId,
    createCamera,
    updateCamera,
    deleteCamera,
} = require('../controllers/camera');

const router = express.Router();

router.get('/', [auth, administrator], getCameras);

router.get('/:nvrId/amount', [auth, administrator], getAmountOfCamerasByNvrId);

// need to be last in the order
router.get('/:id', [auth, administrator], getCameraByCameraId);

router.post('/', [auth, administrator, validator(ValidatorEnum.camera), actionLogger(LoggerActions.addCamera)], createCamera);

router.put('/:id', [auth, administrator, validator(ValidatorEnum.camera), actionLogger(LoggerActions.updateCamera)], updateCamera);

router.delete('/', [auth, administrator, actionLogger(LoggerActions.deleteCamera)], deleteCamera);

module.exports = router;

// const express = require('express');
// const auth = require('../middleware/auth');
// const levelFive = require('../middleware/levelFive');
// const levelThreeAndFourAndFive = require('../middleware/levelThreeAndFourAndFive');
// const logger = require('../middleware/logger');
// const LOGGER_ACTIONS = require('../models/loggerActions');
// const Camera = require('../models/camera');

// const router = express.Router();

// // get cameras
// router.get('/', [auth, levelThreeAndFourAndFive], async (req, res) => {
//     const cameras = await Camera.getCameras();
//     res.send(cameras);
// });

// // add camera
// router.post('/', [auth, levelFive, logger(LOGGER_ACTIONS.addCamera)], async (req, res) => {
//     const {
//         error
//     } = Camera.validateCamera(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     const camera = new Camera(req.body);
//     const result = await Camera.addCamera(camera);
//     camera.cameraId = result.id;
//     return res.send(camera);
// });

// // update camera
// router.put('/:id', [auth, levelFive, logger(LOGGER_ACTIONS.updateCamera)], async (req, res) => {
//     const {
//         error
//     } = Camera.validateCamera(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     const cameraId = +req.params.id;
//     if (!cameraId) {
//         return res.status(400).send('camera id is required');
//     }

//     const result = await Camera.getCameraById(cameraId);
//     if (!result) {
//         return res.status(400).send('camera not exist');
//     }

//     const camera = new Camera(req.body);
//     await Camera.updateCamera(camera, cameraId);
//     return res.send(camera);
// });

// // delete
// router.delete('/:id', [auth, levelFive, logger(LOGGER_ACTIONS.deleteCamera)], async (req, res) => {
//     const cameraId = +req.params.id;
//     if (!cameraId) {
//         return res.status(400).send('camera id is required');
//     }

//     const camera = await Camera.getCameraById(cameraId);
//     if (!camera) {
//         return res.status(400).send('camera not exist');
//     }

//     await Camera.deleteCameraByCameraId(cameraId);
//     return res.send(camera.cameraId.toString());
// });

// // multiple delete
// router.post('/multiple-delete', [auth, levelFive, logger(LOGGER_ACTIONS.deleteMultipleCameras)], async (req, res) => {
//     const cameraIds = req.body;
//     if (cameraIds.length === 0) {
//         return res.status(200);
//     }

//     const results = [];
//     for (const cameraId of cameraIds) {
//         const result = await Camera.getCameraById(cameraId);
//         if (result) {
//             await Camera.deleteCameraByCameraId(cameraId);
//             results.push(cameraId);
//         }
//     }

//     return res.send(results);
// });

// module.exports = router;