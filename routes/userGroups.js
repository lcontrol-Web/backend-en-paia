const express = require('express');
const auth = require('../middleware/auth');
const teamLeaderAndHigher = require('../middleware/teamLeaderAndHigher');
const validator = require('../middleware/validator');
const actionLogger = require('../middleware/logger');
const administrator = require('../middleware/administrator');
const ValidatorEnum = require('../enums/validatorEnum');
const LoggerActions = require('../enums/loggerActions');

const {
    getUserGroups,
    getUserGroupByUserGroupId,
    getAmountOfUserGroups,
    getUsersByUserGroupId,
    createUserGroup,
    updateUserGroup,
    deleteUserGroup,
} = require('../controllers/userGroup');

const router = express.Router();

router.get('/', [auth, administrator], getUserGroups);

router.get('/amount', [auth, administrator], getAmountOfUserGroups);

// need to be last in the order
router.get('/:id', [auth, administrator], getUserGroupByUserGroupId);

router.get('/:id/users', [auth, teamLeaderAndHigher], getUsersByUserGroupId);

router.post('/', [auth, administrator, validator(ValidatorEnum.userGroup), actionLogger(LoggerActions.addUserGroup)], createUserGroup);

router.put('/:id', [auth, administrator, validator(ValidatorEnum.userGroup), actionLogger(LoggerActions.updateUserGroup)], updateUserGroup);

router.delete('/', [auth, administrator, actionLogger(LoggerActions.deleteUserGroup)], deleteUserGroup);

module.exports = router;

// const express = require('express');
// const auth = require('../middleware/auth');
// const levelFive = require('../middleware/levelFive');
// const logger = require('../middleware/logger');
// const LOGGER_ACTIONS = require('../enums/loggerActions');
// // mconst UserGroup = require('../models/userGroup');

// const router = express.Router();

// router.get('/', [auth, levelFive], async (req, res) => {
//     const userGroups = await UserGroup.getUserGroups();
//     res.send(userGroups);
// });

// // add
// router.post('/', [auth, levelFive, logger(LOGGER_ACTIONS.addUserGroup)], async (req, res) => {
//     const {
//         error
//     } = UserGroup.validateUserGroup(req.body);
//     if (error) { return res.status(400).send(error.details[0].message); }

//     const userGroup = new UserGroup(req.body);
//     const result = await UserGroup.addUserGroup(userGroup);
//     userGroup.userGroupId = result.id;
//     return res.send(userGroup);
// });

// // update
// router.put('/:id', [auth, levelFive, logger(LOGGER_ACTIONS.updateUserGroup)], async (req, res) => {
//     const {
//         error
//     } = UserGroup.validateUserGroup(req.body);
//     if (error) { return res.status(400).send(error.details[0].message); }

//     const userGroupId = +req.params.id;
//     if (!userGroupId) { return res.status(400).send('user group id is required'); }

//     const result = await UserGroup.getUserGroupById(userGroupId);
//     if (!result) { return res.status(400).send('User Group not exist'); }

//     const userGroup = new UserGroup(req.body);
//     await UserGroup.updateUserGroup(userGroup, userGroupId);
//     return res.send(userGroup);
// });

// // delete
// router.delete('/:id', [auth, levelFive, logger(LOGGER_ACTIONS.deleteUserGroup)], async (req, res) => {
//     const userGroupId = +req.params.id;
//     if (!userGroupId) { return res.status(400).send('user group id is required'); }

//     const userGroup = await UserGroup.getUserGroupById(userGroupId);
//     if (!userGroup) { return res.status(400).send('user group not exist'); }

//     await UserGroup.deleteUserGroupByUserGroupId(userGroupId);
//     return res.send(userGroup.userGroupId.toString());
// });

// // multiple delete
// router.post('/multiple-delete', [auth, levelFive, logger(LOGGER_ACTIONS.deleteMultipleUserGroups)], async (req, res) => {
//     const userGroupIds = req.body;

//     if (userGroupIds.length === 0) { return res.status(200); }

//     const results = [];
//     for (const userGroupId of userGroupIds) {
//         const result = await UserGroup.getUserGroupById(userGroupId);
//         if (result) {
//             await UserGroup.deleteUserGroupByUserGroupId(userGroupId);
//             results.push(userGroupId);
//         }
//     }

//     return res.send(results);
// });

// module.exports = router;