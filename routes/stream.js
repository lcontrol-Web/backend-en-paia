const express = require('express');
const auth = require('../middleware/auth');
const investigatorAndHigher = require('../middleware/investigatorAndHigher');

const {
    startStreaming,
    getFilesInsideFolderByInterrogationId,
} = require('../controllers/stream');

const router = express.Router();

router.get('/', [], startStreaming);

router.get('/:id/files-inside-folder', [auth, investigatorAndHigher], getFilesInsideFolderByInterrogationId);

module.exports = router;

// const express = require('express');
// const fs = require('fs');
// const sql = require('mssql/msnodesqlv8');
// const jwtLib = require('jsonwebtoken');
// const logger = require('../utils/logger');
// const auth = require('../middleware/auth');
// const levelThreeAndFourAndFive = require('../middleware/levelThreeAndFourAndFive');
// const jwt = require('../startup/jwt');

// const router = express.Router();

// function checkAuth(token) {
//     const privateKey = jwt.getJwt();
//     const user = jwtLib.verify(token, privateKey);
// }

// router.get('/files-inside-folder/:id', [auth, levelThreeAndFourAndFive], async (req, res) => {
//     const interrogationId = +req.params.id;
//     if (!interrogationId) {
//         return res.status(400).send('interrogation id is required');
//     }

//     const pool = await sql.connect(db);
//     const result = await pool.request()
//         .query(`SELECT sharedFolder
//                 FROM config;`);

//     const {
//         sharedFolder
//     } = result.recordset[0];
//     const fullPath = sharedFolder + interrogationId;
//     const dirFiles = [];

//     try {
//         fs.readdir(fullPath, (err, files) => {
//             if (err && err.message.includes('no such file or directory')) {
//                 return res.status(400).send('directoryNotExist');
//             }
//             if (err) {
//                 logger.error(err.message, err);
//                 return res.status(400).send('cannot read files from shared directory');
//             }

//             files.forEach((file) => {
//                 if (file.endsWith('mp4')) {
//                     dirFiles.push(file);
//                 }
//             });

//             return res.send(dirFiles);
//         });
//     } catch (err) {
//         logger.error(err.message, err);
//         return res.status(400).send('cannot read files from directory');
//     }
// });

// router.get('/', async (req, res, next) => {
//     const {
//         token
//     } = req.query;
//     const {
//         file
//     } = req.query;
//     const {
//         folder
//     } = req.query;

//     if (!token || !file || !folder) {
//         return res.status(400).send('not enought information');
//     }

//     checkAuth(token);

//     // const pool = await sql.connect(db);
//     // const result = await pool.request()
//     // .query(`SELECT sharedFolder
//     //         FROM config;`);
//     // const sharedFolder = result.recordset[0].sharedFolder;
//     // const path = sharedFolder + folder + '\\' + file;

//     const path = 'C:\\Development\\paia\\backend\\video2.mp4';
//     const stat = fs.statSync(path);
//     const fileSize = stat.size;
//     const {
//         range
//     } = req.headers;
//     if (range) {
//         const parts = range.replace(/bytes=/, '').split('-');
//         const start = parseInt(parts[0], 10);
//         const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
//         const chunksize = (end - start) + 1;
//         const file = fs.createReadStream(path, {
//             start,
//             end
//         });

//         const head = {
//             'Content-Range': `bytes ${start}-${end}/${fileSize}`,
//             'Accept-Ranges': 'bytes',
//             'Content-Length': chunksize,
//             'Content-Type': 'video/mp4',
//         };
//         res.writeHead(206, head);
//         file.pipe(res);
//     } else {
//         const head = {
//             'Content-Length': fileSize,
//             'Content-Type': 'video/mp4',
//         };
//         res.writeHead(200, head);
//         fs.createReadStream(path).pipe(res);
//     }
// });

// module.exports = router;