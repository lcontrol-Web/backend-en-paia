const express = require('express');
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');
const investigatorAndHigher = require('../middleware/investigatorAndHigher');
const actionLogger = require('../middleware/logger');
const administrator = require('../middleware/administrator');
const ValidatorEnum = require('../enums/validatorEnum');
const LoggerActions = require('../enums/loggerActions');

const {
    getInterrogationRecordsByRangeDates,
    getAmountOfInterrogationRecordsByRangeDates,
    getTextEditorAndChatMessagesByInterrogationId,
    getPauseAndContinueTimesByInterrogationId,
    sendToArchive,
    sendToBurn,
} = require('../controllers/interrogationRecord');

const router = express.Router();

router.post('/range-dates', [auth, validator(ValidatorEnum.rangeDates)], getInterrogationRecordsByRangeDates);

router.post('/amount/range-dates', [auth, validator(ValidatorEnum.rangeDates)], getAmountOfInterrogationRecordsByRangeDates);

router.get('/:id/text-editor-and-chat-messages', [auth, investigatorAndHigher], getTextEditorAndChatMessagesByInterrogationId);

router.get('/:id/pause-continue-times', [auth, investigatorAndHigher], getPauseAndContinueTimesByInterrogationId);

router.put('/send-to-archive/:id', [auth, investigatorAndHigher], sendToArchive);

router.put('/send-to-burn/:id', [auth, investigatorAndHigher], sendToBurn);

module.exports = router;