const express = require('express');
const auth = require('../middleware/auth');
const administrator = require('../middleware/administrator');
const teamLeaderAndHigher = require('../middleware/teamLeaderAndHigher');
const validator = require('../middleware/validator');
const actionLogger = require('../middleware/logger');
const LoggerActions = require('../enums/loggerActions');
const ValidatorEnum = require('../enums/validatorEnum');

const router = express.Router();

const {
    getUsers,
    getUserByUserId,
    getTeamLeaders,
    getAmountOfUsersByUserGroupId,
    createUser,
    updateUser,
    deleteUser,
} = require('../controllers/user');

router.get('/', [auth, teamLeaderAndHigher], getUsers);

router.get('/team-leaders', [auth, administrator], getTeamLeaders);

router.get('/:userGroupId/amount', [auth, administrator], getAmountOfUsersByUserGroupId);

// need to be last in the order
router.get('/:id', [auth, administrator], getUserByUserId);

router.post('/', [auth, administrator, validator(ValidatorEnum.user), actionLogger(LoggerActions.addUser)], createUser);

router.put('/:id', [auth, administrator, validator(ValidatorEnum.user), actionLogger(LoggerActions.updateUser)], updateUser);

router.delete('/', [auth, administrator, actionLogger(LoggerActions.deleteUser)], deleteUser);

module.exports = router;

// router.get('/', [auth, teamLeaderAndHigher], async (req, res) => {
//     let users = [];

//     switch (req.user.userType) {
//         case userTypes.Administrator:
//             users = await User.getUsers();
//             break;
//         case userTypes.TeamLeader:
//             users = await User.getUsersByTeamLeaderId(req.user.userId);
//             break;
//         default:
//             break;
//     }

//     res.send(users);
// });

// router.get('/team-leaders', [auth, administrator], async (req, res) => {
//     const teamLeaders = await User.getTeamLeaders();
//     res.send(teamLeaders);
// });

// // add user
// router.post('/', [auth, administrator, logger(LOGGER_ACTIONS.addUser)], async (req, res) => {
//     const {
//         error
//     } = User.validateUser(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     const user = new User(req.body);

//     // check if user name is already exist
//     let result = await User.getUserByUserName(user.userName);
//     if (result) {
//         return res.status(400).send('user already exist');
//     }

//     // check if investigator have a team leader
//     const user = await User.getUserById(user.userId);
//     if (user.level === 3 && !user.teamLeaderId) {
//         return res.status(400).send('investigator must have team leader');
//     }

//     result = await User.addUser(user);
//     user.userId = result.id;

//     // get the team leader name
//     if (user.teamLeaderId) {
//         const teamLeader = await User.getUserById(user.teamLeaderId);
//         user.teamLeaderName = teamLeader.userName;
//     } else {
//         user.teamLeaderName = null;
//     }

//     return res.send(user);
// });

// // update
// router.put('/:id', [auth, administrator, logger(LOGGER_ACTIONS.updatedUser)], async (req, res) => {
//     const {
//         error
//     } = User.validateUser(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     const userId = +req.params.id;
//     if (!userId) {
//         return res.status(400).send('user id is required');
//     }

//     let result = await User.getUserById(userId);
//     if (!result) {
//         return res.status(400).send('user not exist');
//     }

//     const user = new User(req.body);

//     // check if user name is already taken
//     result = await User.getUserByUserNameAndUserIdIsDifferent(user.userName, user.userId);
//     if (result) {
//         return res.status(400).send('user name already exist');
//     }

//     // check if investigator have a team leader
//     const user = await User.getUserById(user.userId);
//     if (user.level === 3 && !user.teamLeaderId) {
//         return res.status(400).send('investigator must have team leader');
//     }

//     result = await User.updateUser(user, userId);

//     // get the team leader name
//     if (user.teamLeaderId) {
//         const teamLeader = await User.getUserById(user.teamLeaderId);
//         user.teamLeaderName = teamLeader.userName;
//     } else {
//         user.teamLeaderName = null;
//     }

//     return res.send(user);
// });

// // delete
// router.delete('/:id', [auth, administrator, logger(LOGGER_ACTIONS.deleteUser)], async (req, res) => {
//     const userId = +req.params.id;
//     if (!userId) {
//         return res.status(400).send('user id is required');
//     }

//     const user = await User.getUserById(userId);
//     if (!user) {
//         return res.status(400).send('user not exist');
//     }

//     await User.deleteUserByUserId(userId);
//     return res.send(user.userId.toString());
// });

// // multiple delete
// router.post('/multiple-delete', [auth, administrator, logger(LOGGER_ACTIONS.deleteMultipleUsers)], async (req, res) => {
//     const userIds = req.body;
//     if (userIds.length === 0) {
//         return res.status(200);
//     }

//     const results = [];
//     for (const userId of userIds) {
//         const result = await User.getUserById(userId);
//         if (result) {
//             await User.deleteUserByUserId(userId);
//             results.push(userId);
//         }
//     }

//     return res.send(results);
// });

// module.exports = router;