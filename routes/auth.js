const express = require('express');
const ValidatorEnum = require('../enums/validatorEnum');
const LoggerActions = require('../enums/loggerActions');
const validator = require('../middleware/validator');
const actionLogger = require('../middleware/logger');

const {
    authenticateUser,
} = require('../controllers/auth');

const router = express.Router();

router.post('/', [actionLogger(LoggerActions.loggedIn), validator(ValidatorEnum.login)], authenticateUser);

module.exports = router;

// const {
//     LoggedUser,
//     validateLoginCredentials,
//     validateUserName,
// } = require('../models/user');

// const LOGGER_ACTIONS = require('../models/loggerActions');
// const logger = require('../utils/logger');

// const router = express.Router();

// router.post('/', [actionLogger(LOGGER_ACTIONS.loggedIn)], async (req, res) => {
//     const {
//         error
//     } = validateLoginCredentials(req.body);
//     if (error) { return res.status(400).send(error.details[0].message); }

//     const result = await LoggedUser.getUserByUserNameForAuth(req.body.userName);
//     if (!result) { return res.status(400).send('invalid user name or password'); }

//     const loggedUser = new LoggedUser(result);

//     const token = loggedUser.generateAuthToken();
//     res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');
//     req.user = loggedUser;
//     return res.set('x-auth-token', token).send(loggedUser);

//     // const ad = activeDirectory.getAd();
//     // const adGroupName = await activeDirectory.getAdGroupName();
//     // ad.isUserMemberOf(loggedUser.userName, adGroupName, (err, isMember) => {

//     //     if (err) {
//     //         logger.error(err.message, err);
//     //         return res.status(400).send('user is not member of the group');
//     //     }

//     //     if (!isMember)
//     //         return res.status(400).send('user is not member of the group');

//     //     ad.findUser(loggedUser.userName, (err, user) => {
//     //         if (err) {
//     //             logger.error(err.message, err);
//     //             return res.status(400).send('cannot find user in active directory');
//     //         }

//     //         if (!user || !user.userPrincipalName)
//     //             return res.status(400).send('cannot find user in active directory');

//     //         // @123456789#!
//     //         let userId = '@' + user.userPrincipalName.substring(0, user.userPrincipalName.indexOf('@')) + '#!';

//     //         // check password againts the id of the user
//     //         if (userId != req.body.password)
//     //             return res.status(400).send('Invalid user name or password');

//     //         const token = loggedUser.generateAuthToken();
//     //         res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');

//     //         // add to the request the logged user for saving in the logs table
//     //         req.user = loggedUser;

//     //         res.set('x-auth-token', token).send(loggedUser);

//     //     });

//     // });
// });

// const corsOptions = {
//     origin: `http://${process.env.EITAN_SERVER_IP}`
// };

// router.post('/generate-token', cors(corsOptions), async (req, res) => {
//     let ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
//     if (ip.substr(0, 7) === '::ffff:') {
//         ip = ip.substr(7);
//     }

//     if (ip !== process.env.EITAN_SERVER_IP) { return res.status(401).send('Access denied'); }

//     const {
//         error
//     } = validateUserName(req.body);
//     if (error) { return res.status(400).send(error.details[0].message); }

//     const foundedUser = await LoggedUser.getUserByUserNameForAuth(req.body.userName);
//     if (!foundedUser) { return res.status(400).send('User not found'); }

//     const user = new LoggedUser(foundedUser);
//     const token = user.generateAuthToken();
//     return res.status(200).send(token);
// });

// module.exports = router;