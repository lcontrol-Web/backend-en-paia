const express = require('express');
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');
const teamLeaderAndHigher = require('../middleware/teamLeaderAndHigher');
const ValidatorEnum = require('../enums/validatorEnum');

const {
    getLogsByRangeDates,
    getAmountOfLogsByRangeDates,
} = require('../controllers/log');

const router = express.Router();

router.post('/range-dates', [auth, teamLeaderAndHigher, validator(ValidatorEnum.rangeDates)], getLogsByRangeDates);

router.post('/amount/range-dates', [auth, teamLeaderAndHigher, validator(ValidatorEnum.rangeDates)], getAmountOfLogsByRangeDates);

module.exports = router;

// const express = require('express');
// const auth = require('../middleware/auth');
// const levelFive = require('../middleware/levelFive');
// const levelFourAndFive = require('../middleware/levelFourAndFive');

// const router = express.Router();
// const Log = require('../models/log');
// const RangeDates = require('../models/rangeDates');

// router.get('/', [auth, levelFive], async (req, res) => {
//     const logs = await Log.getLogs();
//     res.send(logs);
// });

// router.post('/', [auth, levelFourAndFive], async (req, res) => {
//     const {
//         error
//     } = RangeDates.validateRangeDates(req.body);
//     if (error) { return res.status(400).send(error.details[0].message); }

//     const rangeDates = new RangeDates(new Date(req.body[0]), new Date(req.body[1]));

//     let logs = [];

//     switch (req.user.level) {
//         case 5:
//             logs = await Log.getAllLogsByRangeDates(rangeDates);
//             break;
//         case 4:
//             logs = await Log.getAlllowedLogsByRangeDates(rangeDates, req.user.userId);
//             break;
//         default:
//             break;
//     }

//     return res.send(logs);
// });

// module.exports = router;