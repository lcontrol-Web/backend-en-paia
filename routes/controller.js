const express = require('express');
const auth = require('../middleware/auth');
const investigatorAndHigher = require('../middleware/investigatorAndHigher');

const {
    pingIp,
    getOutputStatusByCameraId,
    turnOnOffOutput,
} = require('../controllers/controller');

const router = express.Router();

router.get('/:ip/ping', [auth, investigatorAndHigher], pingIp);

router.get('/:cameraId/output-status', [auth, investigatorAndHigher], getOutputStatusByCameraId);

router.post('/:cameraId/turn-on-off-output', [auth, investigatorAndHigher], turnOnOffOutput);

module.exports = router;
