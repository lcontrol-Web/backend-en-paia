const express = require('express');
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');
const investigatorAndHigher = require('../middleware/investigatorAndHigher');
const actionLogger = require('../middleware/logger');
const administrator = require('../middleware/administrator');
const ValidatorEnum = require('../enums/validatorEnum');
const LoggerActions = require('../enums/loggerActions');


const {
    getInterrogations,
    getOpenInterrogations,
    getInterrogationByInterrogationId,
    closeActiveInterrogation,
    startInterrogation,
    endInterrogation,
    pauseInterrogation,
    continueInterrogation,
    deleteInterrogation,
} = require('../controllers/interrogation');

const router = express.Router();

router.get('/', [auth, investigatorAndHigher], getInterrogations);

router.get('/open', [auth, investigatorAndHigher], getOpenInterrogations);

router.get('/open', [auth, investigatorAndHigher], getOpenInterrogations);

router.get('/:socketId/close-active-interrogation', [auth, administrator], closeActiveInterrogation);

// need to be last in the order
router.get('/:id', [auth, administrator], getInterrogationByInterrogationId);

router.post('/start', [auth, investigatorAndHigher, validator(ValidatorEnum.interrogation), actionLogger(LoggerActions.startInterrogation)], startInterrogation);

router.put('/end/:id', [auth, investigatorAndHigher, validator(ValidatorEnum.interrogationEnd), actionLogger(LoggerActions.endInterrogation)], endInterrogation);

router.put('/pause/:id', [auth, investigatorAndHigher, validator(ValidatorEnum.interrogationPause), actionLogger(LoggerActions.pauseInterrogation)], pauseInterrogation);

router.put('/continue/:id', [auth, investigatorAndHigher, actionLogger(LoggerActions.continueInterrogation)], continueInterrogation);

router.delete('/', [auth, administrator, actionLogger(LoggerActions.deleteInterrogation)], deleteInterrogation);

module.exports = router;

// const express = require('express');
// const ModbusRTU = require('modbus-serial');
// const auth = require('../middleware/auth');
// const levelFive = require('../middleware/levelFive');
// const levelThreeAndFourAndFive = require('../middleware/levelThreeAndFourAndFive');
// const notLevelOne = require('../middleware/notLevelOne');
// const actionLogger = require('../middleware/logger');
// const LOGGER_ACTIONS = require('../models/loggerActions');

// const {
//     disconnectSocket
// } = require('../startup/socket');

// const Camera = require('../models/camera');
// const {
//     Interrogation
// } = require('../models/interrogation');
// const {
//     InterrogationEnd
// } = require('../models/interrogation');
// const {
//     InterrogationPause
// } = require('../models/interrogation');

// const RangeDates = require('../models/rangeDates');

// const router = express.Router();

// router.get('/', [auth, levelThreeAndFourAndFive], async (req, res) => {
//     const interrogations = await Interrogation.getInterrogations();
//     res.send(interrogations);
// });

// router.get('/open-interrogations', [auth, levelThreeAndFourAndFive], async (req, res) => {
//     const openInterrogations = await Interrogation.getOpenInterrogations();
//     res.send(openInterrogations);
// });

// router.post('/by-range-dates', [auth, notLevelOne], async (req, res) => {
//     const {
//         error
//     } = RangeDates.validateRangeDates(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     const rangeDates = new RangeDates(new Date(req.body[0]), new Date(req.body[1]));
//     let interrogations = [];

//     switch (req.user.level) {
//         case 5:
//         case 2:
//             interrogations = await Interrogation.getInterrogationsByRangeDates(rangeDates);
//             break;
//         case 4:
//             interrogations = await Interrogation.getInterrogationsByRangeDatesAndByUserIdAndByConnectedUsers(rangeDates, req.user.userId);
//             break;
//         case 3:
//             interrogations = await Interrogation.getInterrogationsByRangeDatesAndByUserId(rangeDates, req.user.userId);
//             break;
//         default:
//             break;
//     }

//     return res.send(interrogations);
// });

// router.post('/start-interrogation', [auth, levelThreeAndFourAndFive, actionLogger(LOGGER_ACTIONS.startInterrogation)], async (req, res) => {
//     const {
//         error
//     } = Interrogation.validateInterrogation(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     const interrogation = new Interrogation(req.body);
//     interrogation.userId = req.user.userId;
//     const result = await Interrogation.startInterrogation(interrogation);
//     return res.send(result.id.toString());
// });

// router.put('/end-interrogation/:id', [auth, levelThreeAndFourAndFive, actionLogger(LOGGER_ACTIONS.endInterrogation)], async (req, res) => {
//     const {
//         error
//     } = InterrogationEnd.validateInterrogationEnd(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     const interrogationId = +req.params.id;
//     if (!interrogationId) {
//         return res.status(400).send('interrogation id is required');
//     }

//     const interrogation = await Interrogation.getInterrogationById(interrogationId);
//     if (!interrogation) {
//         return res.status(400).send('interrogation not exist');
//     }

//     const interrogationEnd = new InterrogationEnd(req.body);
//     await Interrogation.endInterrogation(interrogationEnd, interrogationId);
//     return res.send(interrogationEnd);
// });

// router.put('/pause-interrogation/:id', [auth, levelThreeAndFourAndFive, actionLogger(LOGGER_ACTIONS.pauseInterrogation)], async (req, res) => {
//     const {
//         error
//     } = InterrogationPause.validateInterrogationPause(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     const interrogationId = +req.params.id;
//     if (!interrogationId) {
//         return res.status(400).send('interrogation id is required');
//     }

//     const interrogation = await Interrogation.getInterrogationById(interrogationId);
//     if (!interrogation) {
//         return res.status(400).send('interrogation not exist');
//     }

//     const interrogationPause = new InterrogationPause(req.body);
//     await Interrogation.pauseInterrogation(interrogationPause, interrogationId);
//     return res.send(interrogationPause);
// });

// router.put('/continue-interrogation/:id', [auth, levelThreeAndFourAndFive, actionLogger(LOGGER_ACTIONS.continueInterrogation)], async (req, res) => {

// });

// router.get('/ping/:ip', [auth, levelThreeAndFourAndFive], async (req, res) => {
//     const {
//         ip
//     } = req.params;
//     ping.sys.probe(ip, (isAlive) => {
//         res.send(isAlive);
//     });
// });

// router.get('/close-live-interrogation/:socketId', [auth, levelFive], async (req, res) => {
//     const {
//         socketId
//     } = req.params;

//     if (socketId) {
//         return res.status(400).send('socket id is required');
//     }

//     const status = disconnectSocket(socketId);
//     return res.send({
//         status
//     });
// });

// module.exports = router;