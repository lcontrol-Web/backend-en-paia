const express = require('express');
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');
const actionLogger = require('../middleware/logger');
const administrator = require('../middleware/administrator');
const ValidatorEnum = require('../enums/validatorEnum');
const LoggerActions = require('../enums/loggerActions');

const {
    getNvrs,
    getNvrByNvrId,
    getAmountOfNvrs,
    getCamerasByNvrId,
    createNvr,
    updateNvr,
    deleteNvr,
} = require('../controllers/nvr');

const router = express.Router();

router.get('/', [auth, administrator], getNvrs);

router.get('/amount', [auth, administrator], getAmountOfNvrs);

// need to be last in the order
router.get('/:id', [auth, administrator], getNvrByNvrId);

router.get('/:id/cameras', [auth, administrator], getCamerasByNvrId);

router.post('/', [auth, administrator, validator(ValidatorEnum.nvr), actionLogger(LoggerActions.addNvr)], createNvr);

router.put('/:id', [auth, administrator, validator(ValidatorEnum.nvr), actionLogger(LoggerActions.updateNvr)], updateNvr);

router.delete('/', [auth, administrator, actionLogger(LoggerActions.deleteNvr)], deleteNvr);

module.exports = router;

// const express = require('express');
// const auth = require('../middleware/auth');
// const levelFive = require('../middleware/levelFive');
// const levelThreeAndFourAndFive = require('../middleware/levelThreeAndFourAndFive');
// const logger = require('../middleware/logger');
// const LOGGER_ACTIONS = require('../models/loggerActions');
// const Nvr = require('../models/nvr');

// const router = express.Router();

// // get nvrs
// router.get('/', [auth, levelThreeAndFourAndFive], async (req, res) => {
//     const nvrs = await Nvr.getNvrs();
//     res.send(nvrs);
// });

// // add
// router.post('/', [auth, levelFive, logger(LOGGER_ACTIONS.addNvr)], async (req, res) => {
//     const {
//         error
//     } = Nvr.validateNvr(req.body);
//     if (error) { return res.status(400).send(error.details[0].message); }

//     const nvr = new Nvr(req.body);
//     const result = await Nvr.addNvr(nvr);
//     nvr.nvrId = result.id;
//     return res.send(nvr);
// });

// // update
// router.put('/:id', [auth, levelFive, logger(LOGGER_ACTIONS.updateNvr)], async (req, res) => {
//     const {
//         error
//     } = Nvr.validateNvr(req.body);
//     if (error) { return res.status(400).send(error.details[0].message); }

//     const nvrId = +req.params.id;
//     if (!nvrId) { return res.status(400).send('nvr id is required'); }

//     const result = await Nvr.getNvrById(nvrId);
//     if (!result) { return res.status(400).send('nvr not exist'); }

//     const nvr = new Nvr(req.body);
//     await Nvr.updateNvr(nvr, nvrId);
//     return res.send(nvr);
// });

// // delete
// router.delete('/:id', [auth, levelFive, logger(LOGGER_ACTIONS.deleteNvr)], async (req, res) => {
//     const nvrId = +req.params.id;
//     if (!nvrId) { return res.status(400).send('nvr id is required'); }

//     const nvr = await Nvr.getNvrById(nvrId);
//     if (!nvr) { return res.status(400).send('nvr not exist'); }

//     await Nvr.deleteNvrByNvrId(nvrId);
//     return res.send(nvr.nvrId.toString());
// });

// // multiple delete
// router.post('/multiple-delete', [auth, levelFive, logger(LOGGER_ACTIONS.deleteMultipleNvrs)], async (req, res) => {
//     const nvrIds = req.body;

//     if (nvrIds.length === 0) { return res.status(200); }

//     const results = [];
//     for (const nvrId of nvrIds) {
//         const result = await Nvr.getNvrById(nvrId);
//         if (result) {
//             await Nvr.deleteNvrByNvrId(nvrId);
//             results.push(nvrId);
//         }
//     }

//     res.send(results);
// });

// module.exports = router;