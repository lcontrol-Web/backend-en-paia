const express = require('express');

const app = express();
const http = require('http');

const server = http.createServer(app);
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');

const io = require('socket.io')(server, {
    cors: {
        origin: '*',
    }
});

require('express-async-errors');
const logger = require('./utils/logger');

require('dotenv').config({
    path: path.join(__dirname, '/.env'),
});

if (process.env.NODE_ENV) {
    console.log(`Enviroment: ${process.env.NODE_ENV}`);
} else {
    console.log('NODE_ENV not set');
}

app.use(
    express.json({
        limit: '50mb',
    })
);

app.use(
    express.urlencoded({
        limit: '50mb',
        extended: true,
    })
);

// allow access from other services
app.use(cors());
app.use((req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'x-auth-token, X-Requested-With, content-type');
    res.setHeader('Access-Control-Expose-Headers', 'x-auth-token');
    next();
});

if (process.env.NODE_ENV !== 'production') {
    app.use(morgan('dev'));
}

require('./startup/routes')(app);
require('./startup/prod')(app);
require('./startup/jwt')();
require('./startup/socket')(io);

const port = process.env.PORT;
server.listen(port, () => {
    logger.info(`Listening on port ${port}`);
});

module.exports = server;