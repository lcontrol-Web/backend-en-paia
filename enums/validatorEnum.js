const ValidatorEnum = {
    login: 'login',
    log: 'log',
    userGroup: 'userGroup',
    user: 'user',
    nvr: 'nvr',
    camera: 'camera',
    interrogation: 'interrogation',
    interrogationEnd: 'interrogationEnd',
    interrogationPause: 'interrogationPause',
    rangeDates: 'rangeDates',
};

Object.freeze(ValidatorEnum);
module.exports = ValidatorEnum;