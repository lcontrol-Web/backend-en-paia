const LoggerActions = {
    loggedIn: 'logged in',
    addUserGroup: 'addedd user group',
    updateUserGroup: 'updated user group',
    deleteUserGroup: 'deleted user group',
    deleteMultipleUserGroups: 'deleted user groups',
    addUser: 'added user',
    updateUser: 'updated user',
    deleteUser: 'deleted user',
    deleteMultipleUsers: 'deleted users',
    addNvr: 'added NVR',
    updateNvr: 'updated NVR',
    deleteNvr: 'deleted NVR',
    deleteMultipleNvrs: 'deleted NVRS',
    addCamera: 'added camera',
    updateCamera: 'updated camera',
    deleteCamera: 'deleted camera',
    deleteMultipleCameras: 'deleted cameras',
    startInterrogation: 'started interrogation',
    endInterrogation: 'ended interrogation',
    pauseInterrogation: 'paused interrogation',
    deleteInterrogation: 'deleted interrogation',
    continueInterrogation: 'continuted interrogation',
    sendToArchive: 'send to archive',
    sendToBurn: 'send to burn',
};

Object.freeze(LoggerActions);

module.exports = LoggerActions;