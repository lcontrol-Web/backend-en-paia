const SortTypes = {
    Descending: 'desc',
    Ascending: 'asc',
};

Object.freeze(SortTypes);
module.exports = SortTypes;