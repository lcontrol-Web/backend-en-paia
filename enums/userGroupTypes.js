const UserGroupTypes = {
    Administrator: 'administrator',
    TeamLeader: 'team leader',
    Investigator: 'investigator',
    Manager: 'manager',
    NoPermission: 'no permission',
};

Object.freeze(UserGroupTypes);
module.exports = UserGroupTypes;