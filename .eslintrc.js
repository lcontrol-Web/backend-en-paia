module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true,
    },
    extends: [
        'airbnb-base',
    ],
    parserOptions: {
        ecmaVersion: 'latest',
    },
    rules: {
        indent: ['error', 4, {
            SwitchCase: 1
        }],
        'max-len': ['error', {
            code: 250
        }],
        'no-plusplus': 0,
        'nonblock-statement-body-position': ['error', 'below'],
        'implicit-arrow-linebreak': ['warn', 'below'],
        'eol-last': 'off',
        'no-multi-assign': 'off',
        'newline-per-chained-call': ['error', {
            ignoreChainWithDepth: 8
        }],
        'linebreak-style': 'off',
        'no-param-reassign': 0,
        'no-unused-vars': 0,
        'no-console': 'off',
        'comma-dangle': ['error', 'only-multiline'],
    },
};